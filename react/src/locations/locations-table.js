import React from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import { Link } from 'react-router-dom';

/**
 * <TableRowColumn><button onClick={() => { props.delete(props.entry.name, props.entry.id) }}>Verwijderen</button></TableRowColumn>
 */

const Row = (props) => (
        <TableRow key={props.entry.id}>
            <TableRowColumn>{props.entry.id}</TableRowColumn>
            <TableRowColumn>{props.entry.name}</TableRowColumn>
            <TableRowColumn><Link to={"/location/" + props.entry.id + "/status"} >Status</Link></TableRowColumn>
            <TableRowColumn><Link to={"/location/" + props.entry.id + "/problems"}>Problems</Link></TableRowColumn>
        </TableRow>
);

const Rows = (props) => props.entries.map(e => (
    <Row entry={e} delete={props.delete} />
));

const LocationsTable = (props) => (

    <Table
    showRowHover={true}
    stripedRows={true}>
        <TableHeader
            adjustForCheckbox={false}
            displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn>#</TableHeaderColumn>
                    <TableHeaderColumn>Name</TableHeaderColumn>
                    <TableHeaderColumn>Status</TableHeaderColumn>
                    <TableHeaderColumn>Problems</TableHeaderColumn>
                </TableRow>
        </TableHeader>
        <TableBody
            showRowHover={true}>
            <Rows entries={props.entries} delete={props.delete} />
        </TableBody>
    </Table>
)


export default LocationsTable;
