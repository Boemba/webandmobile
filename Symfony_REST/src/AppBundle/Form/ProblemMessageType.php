<?php

namespace AppBundle\Form;

use AppBundle\Entity\Entity\Problem_message;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 12/11/2017
 * Time: 15:01
 */
class ProblemMessageType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('locationId', EntityType::class, array(
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'class' => 'AppBundle\\Entity\\Entity\\Location',
                'empty_data' => '',
                'choice_label' => 'name',
                'label' => 'Location'))
            ->add('date', DateTimeType::class)
            ->add('problem', TextareaType::class)
            ->add('image', FileType::class, array('label' => 'Image (.jpg/.png/.gif)'))
            ->add('solved', ChoiceType::class, array(
                'choices' => array(
                    'Yes' => "Yes",
                    'No' => "No"
                )
            ))
            ->add('save', SubmitType::class,
                array('label' => 'Save Problem'));
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Problem_message::class,
        ));
    }
}