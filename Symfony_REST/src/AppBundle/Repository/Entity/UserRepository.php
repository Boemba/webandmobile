<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 20/10/2017
 * Time: 16:55
 */

namespace AppBundle\Repository\Entity;

use AppBundle\Entity\Entity\User;

/**
 * UserRepository
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{

    /**
     * @param $role
     * @return User | array
     */
    public function findByRoles($role) {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('user')
            ->from(User::class, 'user')
            ->Where('user.roles LIKE :roles')
            ->setParameter('roles', '%"' . $role . '"%')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $email
     * @return User | object
     */
    public function findByEmail($email) {
        return $this->getEntityManager()
            ->getRepository(User::class)
            ->findOneBy(array('email' => $email));
    }
}