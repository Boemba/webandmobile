<?php

namespace AppBundle\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Status_message
 *
 * @ORM\Table(name="status_message")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Entity\Status_messageRepository")
 */
class Status_message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="location_id", type="integer")
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumn(name="locationId", referencedColumnName="id")
     */
    private $locationId;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * status_message constructor.
     * @param int $id
     * @param int $locationId
     * @param string $status
     * @param \DateTime $date
     */
    public function __construct($id, $locationId, $status, \DateTime $date)
    {
        $this->id = $id;
        $this->locationId = $locationId;
        $this->status = $status;
        $this->date = $date;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locationId
     *
     * @param integer $locationId
     *
     * @return status_message
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId
     *
     * @return int
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return status_message
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatusColor($status)
    {
        $status = strtolower($status);
        switch($status) {
            case "goed":
                return "success";
                break;
            case "middelmatig":
                return "warning";
                break;
            case "niet goed":
                return "danger";
                break;
            default:
                return "success";
        }
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return status_message
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDateInProperStringForm()
    {
        return $this->getDate()->format('l jS F Y');
    }
}

