<?php

namespace AppBundle\Entity\Entity;

/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 12/10/2017
 * Time: 9:25
 */

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @package AppBundle\Entity\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Entity\UserRepository")
 * @ORM\Table(name="`user`")
 */

class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        //can add own logic here
    }


}