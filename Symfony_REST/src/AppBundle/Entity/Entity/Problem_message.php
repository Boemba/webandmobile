<?php

namespace AppBundle\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Problem_message
 *
 * @ORM\Table(name="problem_message")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Entity\Problem_messageRepository")
 */
class Problem_message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="location_id", type="integer")
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumn(name="locationId", referencedColumnName="id")
     */
    private $locationId;

    /**
     * @var int
     *
     * @ORM\Column(name="technician_id", type="integer")
     * @ORM\ManyToOne(targetEntity="user")
     * @ORM\JoinColumn(name="technicianId", referencedColumnName="id")
     */
    private $technicianId;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="problem", type="string", length=255)
     */
    private $problem;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string")
     *
     * @Assert\NotBlank(message="Please, upload an image. (.jpg/.png/.gif)")
     */
    private $image;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="solved", type="string", length=255)
     */
    private $solved;

    /**
     * Problem_message constructor.
     * @param int $id
     * @param $technicianId
     * @param int $locationId
     * @param string $problem
     * @param string $image
     * @param \DateTime $date
     * @param string $solved
     */
    public function __construct($id, $technicianId, $locationId, $problem, $image, \DateTime $date, $solved)
    {
        $this->id = $id;
        $this->locationId = $locationId;
        $this->technicianId = $technicianId;
        $this->problem = $problem;
        $this->date = $date;
        $this->solved = $solved;
        $this->image = $image;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set locationId
     *
     * @param integer $locationId
     *
     * @return Problem_message
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId
     *
     * @return int
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @return int
     */
    public function getTechnicianId()
    {
        return $this->technicianId;
    }

    /**
     * @param int $technicianId
     */
    public function setTechnicianId($technicianId)
    {
        $this->technicianId = $technicianId;
    }

    /**
     * Set problem
     *
     * @param string $problem
     *
     * @return Problem_message
     */

    public function setProblem($problem)
    {
        $this->problem = $problem;

        return $this;
    }

    /**
     * Get problem
     *
     * @return string
     */
    public function getProblem()
    {
        return $this->problem;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Set date
     *
     * @param string $date
     *
     * @return Problem_message
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set solved
     *
     * @param string $solved
     *
     * @return Problem_message
     */
    public function setSolved($solved)
    {
        $this->solved = $solved;

        return $this;
    }

    /**
     * Get solved
     *
     * @return string
     */
    public function getSolved()
    {
        return $this->solved;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDateInProperStringForm()
    {
        return $this->getDate()->format('l jS F Y');
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getSolvedColor($solved)
    {
        if(strtolower($solved) == "yes") {
            return "success";
        } else {
            return "danger";
        }
    }
}

