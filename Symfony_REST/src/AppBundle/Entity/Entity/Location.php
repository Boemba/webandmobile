<?php

namespace AppBundle\Entity\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Location
 *
 * @ORM\Table(name="location")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Entity\LocationRepository")
 */
class Location
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    private $problem_messages;
    private $status_messages;

    /**
     * @return mixed
     */
    public function getProblemMessages()
    {
        return $this->problem_messages;
    }

    /**
     * @param mixed $problem_messages
     */
    public function setProblemMessages(array $problem_messages)
    {
        $this->problem_messages = $problem_messages;
    }

    /**
     * @return mixed
     */
    public function getStatusMessages()
    {
        return $this->status_messages;
    }

    /**
     * @param mixed $status_messages
     */
    public function setStatusMessages(array $status_messages)
    {
        $this->status_messages = $status_messages;
    }



    /**
     * Location constructor.
     * @param int $id
     * @param string $name
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Location
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    function __toString()
    {
        return $this->getId() . '';
    }


}

