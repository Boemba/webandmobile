<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LocationControllerTest extends WebTestCase
{
    public function testAllLocationGet() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/location/all');

        $this->assertGreaterThan(
            0,
            $crawler->filter('html:contains("All Locations")')->count()
        );
    }

    public function testLocationWithNoIdGet() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/location');

        $this->assertGreaterThan(
            0,
            $crawler->filter('html:contains("Status messages of <b>PXL - BE041</b>")')->count()
        );
    }

    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful($url) {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function urlProvider() {
        return array(
            array('/'),
            array('/locations/all'),
            array('/location'),
            array('/register/')
        );
    }
}
