<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Entity\Problem_message;
use AppBundle\Entity\Entity\User;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WerkbeheerderController extends Controller
{
    /**
     * @Route("/manager/TechnicianAssigned", name="assignProblemToTechnician")
     */
    public function assignProblemToTechnicianAction(Request $request)
    {
        $technicianId = $request->request->get("technicianId");
        $problemMessageId = $request->request->get("problemMessageId");

        /**@var Problem_message | $problemMessage **/
        $problemMessage = $this->getDoctrine()
            ->getRepository(Problem_message::class)
            ->find($problemMessageId);

        $em = $this->getDoctrine()->getManager();

        $problemMessage->setTechnicianId($technicianId);
        $em->flush();

        /**@var User | $technician **/
        $technician = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($technicianId);

        $message = \Swift_Message::newInstance()
            ->setSubject('new problem assigned to you!')
            ->setFrom('alessio_marzo@hotmail.com')
            ->setTo($technician->getEmail())
            ->setBody("Hello technician! \n \n" . "You are assigned to: " . $problemMessage->getProblem());

        $mailer = $this->container->get('mailer');

        $mailer->send($message);

        return $this->redirect($this->generateUrl('location', array('locationId' => $problemMessage->getLocationId())), 301);
    }

    /**
     * @Route("/manager/deleteTechnician", name="deleteTechnicianFromProblem")
     */
    public function deleteTechnicianFromProblemAction(Request $request)
    {
        $problemMessageId = $request->query->get("problemMessageId");

        /**@var Problem_message | $problemMessage **/
        $problemMessage = $this->getDoctrine()
            ->getRepository(Problem_message::class)
            ->find($problemMessageId);

        $em = $this->getDoctrine()->getManager();

        $problemMessage->setTechnicianId(null);
        $em->flush();

        return $this->redirect($this->generateUrl('location', array('locationId' => $problemMessage->getLocationId())), 301);
    }

}
