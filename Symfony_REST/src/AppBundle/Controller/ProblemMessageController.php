<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 12/10/2017
 * Time: 21:14
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Entity\Problem_message;
use AppBundle\Form\ProblemMessageType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProblemMessageController extends Controller
{
    /**
     * @Route("problems/new", name="new_problem")
     */
    public function newProblemAction(Request $request) {
        $problem = new Problem_message(null, 0, 0, "", "", new \DateTime('now'), "No");

        $form = $this->createForm(ProblemMessageType::class, $problem);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            /** @var File $file */
            $file = $problem->getImage();
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            $file->move(
                $this->getParameter('problem_images'),
                $fileName
            );

            $problem->setImage($fileName);
            $problem->setTechnicianId(0);

            $validator = $this->get('validator');
            $errors = $validator->validate($problem);

            if(count($errors) > 0) {
                return new Response((string)$errors);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($problem);
            $em->flush();
            return $this->redirect($this->generateUrl('location', array('locationId' => $problem->getLocationId())), 301);
        } else {
            return $this->render('problem/new_problem.html.twig',
                array('form' => $form->createView()));
        }
    }

}