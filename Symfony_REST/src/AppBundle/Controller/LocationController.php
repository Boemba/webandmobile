<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Entity\Location;
use AppBundle\Entity\Entity\Problem_message;
use AppBundle\Entity\Entity\Status_message;
use AppBundle\Entity\Entity\User;
use AppBundle\Repository\Entity\Problem_messageRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LocationController extends Controller
{
    /**
     * @Route("/location/{locationId}",
     *      name="location",
     *      defaults={"locationId": "1"},
     *      requirements={"locationId": "\d+"})
     */
    public function showLocationByIdAction($locationId) {
        $location = $this->getDoctrine()
            ->getRepository(Location::class)
            ->find($locationId);

        return $this->render('location/location.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'location' => $location
        ]);
    }

    /**
     * @Route("/location/all/{pageNumber}",
     *      name="all_locations",
     *      requirements={"pageNumber": "\d+"},
     *      defaults={"pageNumber" = "1"})
     */
    public function showAllLocationsAction($pageNumber) {
        $locations = $this->getDoctrine()
            ->getRepository(Location::class)
            ->findAll();

        return $this->render('location/locations.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'locations' => $locations
        ]);
    }

    /**
     * @Route("", name="problem_message_listdetails")
     */
    public function problemMessageListDetailsAction($locationId) {
            $problemMessages = $this->getDoctrine()
                ->getRepository(Problem_message::class)
                ->findByLocationId($locationId, 5);

            return $this->render('location/list_details/problem_message_listdetails.html.twig', [
                'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
                'problemMessages' => $problemMessages
            ]);
    }

    /**
     * @Route("", name="status_message_listdetails")
     */
    public function statusMessageListDetailsAction($locationId) {
        $problemMessages = $this->getDoctrine()
            ->getRepository(Status_message::class)
            ->findByLocationId($locationId, 5);

        return $this->render('location/list_details/status_message_listdetails.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'statusMessages' => $problemMessages
        ]);
    }

    /**
     * @Route("/status/{statusId}", name="status_message_details")
     */
    public function statusMessageDetailsAction($statusId) {
       $statusDetails = $this->getDoctrine()
           ->getRepository(Status_message::class)
           ->find($statusId);

       $locationDetails = $this->getDoctrine()
           ->getRepository(Location::class)
           ->find($statusDetails->getLocationId());

        return $this->render('location/status_details.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'status_details' => $statusDetails,
            'location_details' => $locationDetails
        ]);

    }

    /**
     * @Route("", name="status_message")
     */
    public function statusMessageAction($locationId) {
        $statusMessages = $statusMessages = $this->getDoctrine()
            ->getRepository(Status_message::class)
            ->findByLocationId($locationId, 10);

        return $this->render('location/status_message.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'status_messages' => $statusMessages
        ]);
    }

    /**
     * @Route("", name="problem_message")
     */
    public function problemMessageAction($locationId) {
        $problemMessages = $problemMessages = $this->getDoctrine()
            ->getRepository(Problem_message::class)
            ->findByLocationId($locationId, 10);

        $allTechnicians = $this->getDoctrine()
            ->getRepository(User::class)
            ->findByRoles('ROLE_TECH');

        return $this->render('location/problem_message.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'problem_messages' => $problemMessages,
            'technicians' => $allTechnicians
        ]);
    }
}
