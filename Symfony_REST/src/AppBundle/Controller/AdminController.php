<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 20/10/2017
 * Time: 16:44
 */

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Repository\Entity\UserRepository;
use Doctrine\ORM\Mapping\Entity;
use AppBundle\Entity\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Role\Role;

class AdminController extends Controller
{
    /**
     * @Route("/admin/", name="admin_area")
     */
    public function adminAction(Request $request) {
        /** @var UserRepository | $userRepository */
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $allTechnicians = $userRepository->findByRoles('ROLE_TECH');

        $unsuccessfully_added_tech = $request->get('unsuccessfully_added_tech');

        return $this->render('admin/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'technicians_list' => $allTechnicians,
            'unsuccessfully_added_tech' => $unsuccessfully_added_tech,
        ]);
    }

    /**
     * @Route("/admin/add", name="admin_add_technician")
     */
    public function addTechnicianAction(Request $request) {
        $formData = $request->get('fos_user_registration_form');
        $email = $formData['email'];
        $username = $formData['username'];
        $password = $formData['plainPassword']['first'];

        $successfullyRegistered = $this->registerTechnician($email,$username,$password);

        if($successfullyRegistered) {
            return $this->redirect($this->generateUrl('admin_area'));
        } else {
            return $this->redirect($this->generateUrl('admin_area', array('unsuccessfully_added_tech' => 'true')), 301);
        }
    }

    /**
     * @Route("/admin/edit/submit", name="admin_submit_edit_technician")
     */
    public function editTechnicianSubmitAction(Request $request) {
        $formData = $request->get('technician_edit_form');
        $id = $formData['id'];
        $email = $formData['email'];
        $username = $formData['username'];
        $password = $formData['plainPassword']['first'];

        $usersRepository = $this->getDoctrine()->getRepository(User::class);

        $user = $usersRepository->find($id);
        if(\is_null($user)) {
            return $this->redirect($this->generateUrl('admin_area', array('error' => 'User could not be found, please try again or contact Super Admin.')), 301);
        }

        /* @var User|$user */
        $user->setUsername(is_null($username) ? $user->getUsername() : $username);
        $user->setEmail(is_null($email) ? $user->getEmail() : $email);
        $user->setEmailCanonical(is_null($email) ? $user->getEmailCanonical() : $email);
        $user->setEnabled(1); // enable the user or enable it later with a confirmation token in the email
        // this method will encrypt the password with the default settings :)
        $user->setPlainPassword(is_null($password) ? $user->getPassword() : $password);
        $userManager = $this->get('fos_user.user_manager');
        $userManager->updateUser($user);

        return $this->redirect($this->generateUrl('admin_area', array('error' => 'User is edited')), 301);
    }

    /**
     * @Route("/admin/edit/{id}", name="admin_edit_technician")
     */
    public function editTechnicianAction(Request $request, $id) {
        $usersRepository = $this->getDoctrine()->getRepository(User::class);

        $technician = $usersRepository->find($id);
        if(\is_null($technician)) {
            return $this->redirect($this->generateUrl('admin_area', array('error' => 'Technician could not be found, please try again or contact Super Admin.')), 301);
        }

        return $this->render('admin/edit.technician.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'technician' => $technician,
        ]);
    }

    /**
     * @Route("/admin/remove", name="admin_remove_technician")
     */
    public function removeTechnicianAction(Request $request) {
        $email = $request->get('email');

        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository(User::class)->findByEmail($email);
        if(\is_null($user)) {
            return $this->redirect($this->generateUrl('admin_area', array('error' => 'User could not be deleted, please contact super admin.')), 301);
        }

        $em->remove($user);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_area', array('error' => 'User is deleted')), 301);
    }

    private function registerTechnician($email, $username, $password) {
        $userManager = $this->get('fos_user.user_manager');

        $em = $this->getDoctrine()->getManager();
        $usersRepository = $em->getRepository(User::class);

        $email_exist = $usersRepository->findByEmail($email);

        // Check if the user exists to prevent Integrity constraint violation error in the insertion
        if($email_exist){
            return false;
        }

        /* @var User|$user */
        $user = $userManager->createUser();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setEmailCanonical($email);
        $user->setEnabled(1); // enable the user or enable it later with a confirmation token in the email
        // this method will encrypt the password with the default settings :)
        $user->setPlainPassword($password);
        $user->addRole('ROLE_TECH');
        $userManager->updateUser($user);

        return true;
    }

}