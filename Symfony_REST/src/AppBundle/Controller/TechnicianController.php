<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Entity\Problem_message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class TechnicianController extends Controller
{
    /**
     * @Route("/technician", name="technician_area")
     */
    public function technicianAction()
    {
        $technicianId = $this->getUser()->getId();

        /**@var Problem_message | $problemMessage **/
        $problemMessages = $this->getDoctrine()
            ->getRepository(Problem_message::class)
            ->findByTechnicianId($technicianId);

        return $this->render('Technician/technician.html.twig', array(
            'problemMessages' => $problemMessages
        ));
    }

    /**
     * @Route("/finishedProblem", name="finishedProblem")
     */
    public function finishedProblemAction(Request $request)
    {
        $problemMessageId = $request->query->get("problemMessageId");

        /**@var Problem_message | $problemMessage **/
        $problemMessage = $this->getDoctrine()
            ->getRepository(Problem_message::class)
            ->find($problemMessageId);

        $em = $this->getDoctrine()->getManager();

        $problemMessage->setSolved("Yes");
        $em->flush();

        return $this->redirect($this->generateUrl('technician_area'));
    }
}
