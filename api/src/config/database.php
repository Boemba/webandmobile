<?php

/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 29/09/2017
 * Time: 8:42
 */

return [
    'host' => '127.0.0.1',
    'username' => 'root',
    'password' => '',
    'database' => 'symfony'
];