<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 29/09/2017
 * Time: 9:12
 */

namespace api\view;


interface View
{
    public function show(array $data);
}