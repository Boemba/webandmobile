<?php

/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 29/09/2017
 * Time: 9:11
 */

namespace api\view;

class LocationJsonView implements View
{
    public function show(array $data)
    {
       http_response_code(200);
       header('Content-Type: application/json');
       header('access-control-allow-origin: *');

       if(isset($data['location'])) {
           $location = $data['location'];
           echo json_encode(['id' => $location->getLocationId(),
               'name' => $location->getLocationName()]);
       } else if (isset($data['locations'])) {
           $json = "[";

           foreach ($data['locations'] as $location) {
               $json .= json_encode(['id' => $location->getLocationId(),
                   'name' => $location->getLocationName()]) . ',';
           }

           $json = substr($json, 0, -1) . "]";

           echo $json;
       } else {
           http_response_code(404);
           echo '{}';
       }
    }
}