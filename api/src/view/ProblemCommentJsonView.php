<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 30/09/2017
 * Time: 17:44
 */

namespace api\view;


class ProblemCommentJsonView implements View
{
    public function show(array $data)
    {
        http_response_code(200);
        header('Content-Type: application/json');
        header('access-control-allow-origin: *');

        if(isset($data['problem'])) {
            $problem = $data['problem'];
            $comments_json = [];
            if($data['comments'] != null) {
                foreach ($data['comments'] as $comment) {
                    array_push($comments_json, ['id' => $comment->getId(),
                        'problemId' => $comment->getProblemMessageId(),
                        'problemText' => $comment->getProblemCommentText(),
                        'problemDate' => $comment->getProblemCommentDate()]);
                }
            }

            $comments_json = json_encode($comments_json);

            $full_json = json_encode(['problemId' => $problem->getProblemMessageId(),
                'problemLocationId' => $problem->getProblemMessageLocationId(),
                'problemText' => $problem->getProblemMessageText(),
                'problemDate' => $problem->getProblemMessageDate(),
                'problemIsFinished' => $problem->getProblemMessageIsFinished(),
                'problemComment' => json_decode($comments_json)]);

            echo $full_json;
        } else if(isset($data['comment'])) {
            $comment_json = [];

            if($data['comment'] != null) {
                $comment = $data['comment'];

                array_push($comment_json, ['id' => $comment->getId(),
                    'problemId' => $comment->getProblemMessageId(),
                    'problemText' => $comment->getProblemCommentText(),
                    'problemDate' => $comment->getProblemCommentDate()]);
            }

            echo json_encode(['comment' => $comment_json]);
        } else {
            http_response_code(404);
            echo '{}';
        }
    }
}