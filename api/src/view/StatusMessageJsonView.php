<?php

namespace api\view;

class StatusMessageJsonView implements View
{
    public function show(array $data)
    {
        http_response_code(200);
        header('Content-Type: application/json');
        header('access-control-allow-origin: *');

        if(isset($data['status'])) {
            $status = $data['status'];
            echo json_encode(['statusId' => $status->getStatusMessageId(),
                'statusLocationId' => $status->getStatusMessageLocationId(),
                'statusState' => $status->getStatusMessageState(),
                'statusDate' => $status->getStatusMessageDate()]);
        } else if (isset($data['statuses'])) {
            $json = "[";

            foreach ($data['statuses'] as $status) {
                $json .= json_encode(['statusId' => $status->getStatusMessageId(),
                        'statusLocationId' => $status->getStatusMessageLocationId(),
                        'statusState' => $status->getStatusMessageState(),
                        'statusDate' => $status->getStatusMessageDate()]) . ',';
            }

            $json = substr($json, 0, -1) . "]";

            echo $json;
        } else {
            http_response_code(404);
            echo '{}';
        }
    }
}