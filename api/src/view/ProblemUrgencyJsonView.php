<?php
/**
 * Created by PhpStorm.
 * User: Alessio Marzo
 * Date: 19/10/2017
 * Time: 10:29
 */

namespace api\view;


class ProblemUrgencyJsonView implements View
{
    public function show(array $data)
    {
        http_response_code(200);
        header('Content-Type: application/json');
        header('access-control-allow-origin: *');

        if(isset($data['problem'])) {
            $problem = $data['problem'];
            $problemUrgency = $data['problemUrgency'];
            $problemUrgency_json = null;

            if ($problemUrgency != null) {
                    $problemUrgency_json = json_encode(['id' => $problemUrgency->getProblemId(),
                    'problemId' => $problemUrgency->getProblemId(),
                    'urgencyLevel' => $problemUrgency->getUrgencyLevel()]);
            }
            $full_json = json_encode(['problemId' => $problem->getProblemMessageId(),
                'problemLocationId' => $problem->getProblemMessageLocationId(),
                'problemText' => $problem->getProblemMessageText(),
                'problemDate' => $problem->getProblemMessageDate(),
                'problemIsFinished' => $problem->getProblemMessageIsFinished(),
                'problemUrgency' => json_decode($problemUrgency_json)]);

            echo $full_json;
        } else {
            http_response_code(404);
            echo '{}';
        }
    }
}