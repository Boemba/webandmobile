<?php

/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 29/09/2017
 * Time: 9:11
 */

namespace api\view;

class ProblemMessageJsonView implements View
{
    public function show(array $data)
    {
        http_response_code(200);
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');

        if(isset($data['problem'])) {
            $problem = $data['problem'];
            echo json_encode(['problemId' => $problem->getProblemMessageId(),
                'problemLocationId' => $problem->getProblemMessageLocationId(),
                'problemText' => $problem->getProblemMessageText(),
                'problemDate' => $problem->getProblemMessageDate(),
                'problemIsFinished' => $problem->getProblemMessageIsFinished()]);
        } else if (isset($data['problems'])) {
            $json = "[";

            foreach ($data['problems'] as $problem) {
                $json .= json_encode(['problemId' => $problem->getProblemMessageId(),
                        'problemLocationId' => $problem->getProblemMessageLocationId(),
                        'problemText' => $problem->getProblemMessageText(),
                        'problemDate' => $problem->getProblemMessageDate(),
                        'problemIsFinished' => $problem->getProblemMessageIsFinished()]) . ',';
            }

            $json = substr($json, 0, -1) . "]";

            echo $json;
        } else {
            http_response_code(404);
            echo '{}';
        }
    }
}