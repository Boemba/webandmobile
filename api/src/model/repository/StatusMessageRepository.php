<?php

namespace api\model\repository;

use api\model\entity\StatusMessage;

interface StatusMessageRepository
{
    public function findStatusMessageById($id);
    public function insertStatusMessage(StatusMessage $status);
    public function deleteStatusById($id);
    public function updateStatusById($id);
    public function findStatusMessageByLocationId($id);
}