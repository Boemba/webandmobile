<?php

namespace api\model\repository;

use api\model\entity\ProblemUrgency;

interface ProblemUrgencyRepository
{
    public function findProblemUrgencyByProblemId($id);
    public function insertProblemUrgency(ProblemUrgency $problemUrgency);
    public function updateProblemUrgency(ProblemUrgency $problemUrgency);
}