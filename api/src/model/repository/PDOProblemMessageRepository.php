<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 29/09/2017
 * Time: 11:02
 */
namespace api\model\repository;


use api\model\entity\ProblemComment;
use api\model\entity\ProblemMessage;
use api\view\ProblemMessageJsonView;

class PDOProblemMessageRepository implements ProblemMessageRepository
{

    private $connection = null;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    private function createNewProblemMessageFromResults($results)
    {
        $problem = new ProblemMessage(
            $results['id'],
            $results['location_id'],
            $results['problem'],
            $results['date'],
            $results['solved']
        );

        return $problem;
    }

    public function findAllProblems()
    {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM problem_message");
            $stmt->execute();
            $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) <= 0) {
                return null;
            }

            $problems = [];

            foreach ($results as $problem) {
                $problems[] = $this->createNewProblemMessageFromResults($problem);
            }

            return $problems;
        }
        catch (\Exception $e) {
            return null;
        }
    }

    public function findProblemMessageById($id)
    {
        try {
            // SELECT problem with id
            $stmt = $this->connection->prepare("SELECT * FROM problem_message WHERE id = :id");
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
            $stmt->execute();
            $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) <= 0) {
                return null;
            }

            return $this->createNewProblemMessageFromResults($results[0]);
        }
        catch (\Exception $e) {
            return null;
        }
    }
	
	public function findProblemMessageByLocationId($id) {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM problem_message WHERE location_id = :id");
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
            $stmt->execute();
            $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) <= 0) {
                return null;
            }

            $problems = [];

            foreach ($results as $problem) {
                $problems[] = $this->createNewProblemMessageFromResults($problem);
            }

            return $problems;
        }
        catch (\Exception $e) {
            return null;
        }
	}

    public function insertProblemMessage(ProblemMessage $problem)
    {
        try {
            //INSERT new problemMessage
            $stmt = $this->connection->prepare("INSERT INTO problem_message(location_id, problem, date, solved)
                    VALUES (:location_id, :problem, :date, :solved)");
            $stmt->bindValue(':location_id', $problem->getProblemMessageLocationId());
            $stmt->bindValue(':problem', $problem->getProblemMessageText());
            $stmt->bindValue(':date', $problem->getProblemMessageDate());
            $stmt->bindValue(':solved', $problem->getProblemMessageIsFinished());
            $stmt->execute();

            if ($stmt) {
                $lastId = $this->connection->lastInsertId();

                $stmt = $this->connection->prepare("INSERT INTO problem_urgency(problem_id, urgency_level)
                    VALUES ($lastId, 0)");
                $stmt->execute();

                return new ProblemMessage($lastId,
                    $problem->getProblemMessageLocationId(),
                    $problem->getProblemMessageText(),
                    $problem->getProblemMessageDate(),
                    $problem->getProblemMessageIsFinished());
            }
            return null;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteProblemById($id)
    {
        // TODO: Implement deleteProblemById() method.
    }

    public function updateProblemById($Id)
    {
        // TODO: Implement updateProblemById() method.
    }

}