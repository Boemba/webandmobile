<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 29/09/2017
 * Time: 8:32
 */

namespace api\model\repository;


interface LocationRepository
{
    public function findAllLocations();
    public function findLocationById($id);
}