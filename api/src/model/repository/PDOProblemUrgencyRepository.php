<?php
/**
 * Created by PhpStorm.
 * User: Alessio Marzo
 * Date: 19/10/2017
 * Time: 10:19
 */

namespace api\model\repository;


use api\model\entity\ProblemUrgency;

class PDOProblemUrgencyRepository implements ProblemUrgencyRepository
{

    private $connection = null;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    private function createNewProblemUrgencyFromResults($results)
    {
        $problemUrgency = new ProblemUrgency(
            $results['id'],
            $results['problem_id'],
            $results['urgency_level']
        );

        return $problemUrgency;
    }

    public function findProblemUrgencyByProblemId($id)
    {
        try {
            // SELECT problem_urgency with id
            $stmt = $this->connection->prepare("SELECT * FROM problem_urgency WHERE problem_id = :id");
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
            $stmt->execute();
            $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) <= 0) {
                return null;
            }

            return $this->createNewProblemUrgencyFromResults($results[0]);
        }
        catch (\Exception $e) {
            return null;
        }
    }

    public function insertProblemUrgency(ProblemUrgency $problemUrgency)
    {
        try {
            //INSERT new problemUrgency
            $stmt = $this->connection->prepare("INSERT INTO problem_urgency(problem_id, urgency_level)
                    VALUES (:problem_id, :urgency_level)");
            $stmt->bindValue(':problem_id', $problemUrgency->getProblemId());
            $stmt->bindValue(':urgency_level', $problemUrgency->getUrgencyLevel());
            $stmt->execute();

            if ($stmt) {
                $lastId = $this->connection->lastInsertId();
                return new ProblemUrgency($lastId,
                    $problemUrgency->getProblemId(),
                    $problemUrgency->getUrgencyLevel());
            }
            return null;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function updateProblemUrgency(ProblemUrgency $problemUrgency)
    {
        try {
            //INSERT new problemUrgency
            $stmt = $this->connection->prepare("UPDATE problem_urgency
                    SET urgency_level = :urgency_level
                    WHERE problem_id = :problem_id");
            $stmt->bindValue(':problem_id', $problemUrgency->getProblemId());
            $stmt->bindValue(':urgency_level', $problemUrgency->getUrgencyLevel());
            $stmt->execute();

            if ($stmt) {
                return new ProblemUrgency($problemUrgency->getId(),
                    $problemUrgency->getProblemId(),
                    $problemUrgency->getUrgencyLevel());
            }
            return null;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}