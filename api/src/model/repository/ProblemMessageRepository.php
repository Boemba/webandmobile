<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 29/09/2017
 * Time: 10:53
 */

namespace api\model\repository;

use api\model\entity\ProblemMessage;

interface ProblemMessageRepository
{
    public function findProblemMessageById($id);
    public function findAllProblems();
    public function insertProblemMessage(ProblemMessage $problem);
    public function deleteProblemById($id);
    public function updateProblemById($Id);
    public function findProblemMessageByLocationId($id);
}