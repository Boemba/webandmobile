<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 30/09/2017
 * Time: 18:04
 */

namespace api\model\repository;


interface ProblemCommentRepository
{
    public function findProblemMessageCommentsByProblemId($id);
    public function insertProblemComment($problemComment);
}