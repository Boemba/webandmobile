<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 29/09/2017
 * Time: 8:47
 */

namespace api\model\repository;

use api\model\entity\Location;
use Prophecy\Exception\Doubler\ClassNotFoundException;

class PDOLocationRepository implements LocationRepository
{
    private $connection = null;

    function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    private function createNewLocationFromResults($results)
    {
        $location = new Location($results['id'],
            $results['name']);
        return $location;
    }

    public function findAllLocations()
    {
        try {
            // SELECT all Locations
            $stmt = $this->connection->prepare("SELECT * FROM location");
            $stmt->execute();
            $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            if (count($results) <= 0) {
                return null;
            }

            $locations = [];

            foreach ($results as $location) {
                $locations[] = $this->createNewLocationFromResults($location);
            }
            return $locations;
        }
        catch (\Exception $e) {
            echo $e->getMessage();
            return null;
        }
    }


    public function findLocationById($id)
    {
        try {
            // SELECT user with id
            $stmt = $this->connection->prepare("SELECT * FROM location WHERE id = :id");
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
            $stmt->execute();
            $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) <= 0) {
                return null;
            }

            return $this->createNewLocationFromResults($results[0]);
        }
        catch (\Exception $e) {
            return null;
        }
    }
}