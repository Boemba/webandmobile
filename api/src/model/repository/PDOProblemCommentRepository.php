<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 30/09/2017
 * Time: 18:05
 */

namespace api\model\repository;

use api\model\entity\ProblemComment;
use api\model\entity\ProblemMessage;

class PDOProblemCommentRepository implements ProblemCommentRepository
{

    private $connection = null;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    private function createNewCommentsFromResults($results)
    {
        $comment = new ProblemComment(
            $results['id'],
            $results['problem_id'],
            $results['comment_text'],
            $results['comment_date']
        );

        return $comment;
    }

    public function findProblemMessageCommentsByProblemId($id)
    {
        try {
            // SELECT problem with id
            $stmt = $this->connection->prepare("SELECT * FROM problem_comment WHERE problem_id = :id ORDER BY comment_date");
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
            $stmt->execute();
            $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) <= 0) {
                return null;
            }

            $comments = [];

            foreach ($results as $comment) {
                $comments[] = $this->createNewCommentsFromResults($comment);
            }
            return $comments;
        }
        catch (\Exception $e) {
            return null;
        }
    }


    public function insertProblemComment($problemComment)
    {
        /** @var ProblemComment | $problemComment */
        try {
            //INSERT new problemComment
            $stmt = $this->connection->prepare("INSERT INTO problem_comment(problem_id, comment_text, comment_date)
                    VALUES (:problemId, :commentText, :date)");
            $stmt->bindValue(':problemId', $problemComment->getProblemMessageId());
            $stmt->bindValue(':commentText', $problemComment->getProblemCommentText());
            $stmt->bindValue(':date', $problemComment->getProblemCommentDate());
            $stmt->execute();

            if ($stmt) {
                $lastId = $this->connection->lastInsertId();
                return new ProblemComment($lastId,
                    $problemComment->getProblemMessageId(),
                    $problemComment->getProblemCommentText(),
                    $problemComment->getProblemCommentDate());
            }
            return null;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}