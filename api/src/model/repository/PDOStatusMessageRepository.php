<?php

namespace api\model\repository;

use api\model\entity\StatusMessage;
use api\view\StatusMessageJsonView;

class PDOStatusMessageRepository implements StatusMessageRepository
{

    private $connection = null;

    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    private function createNewStatusMessageFromResults($results)
    {
        $status = new StatusMessage(
            $results['id'],
            $results['location_id'],
            $results['status'],
            $results['date']
        );

        return $status;
    }

    public function findStatusMessageById($id)
    {
        try {
            // SELECT status with id
            $stmt = $this->connection->prepare("SELECT * FROM status_message WHERE id = :id");
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
            $stmt->execute();
            $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) <= 0) {
                return null;
            }

            return $this->createNewStatusMessageFromResults($results[0]);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function insertStatusMessage(StatusMessage $status)
    {
        try {
            //INSERT new statusmessage
            $stmt = $this->connection->prepare("INSERT INTO status_message(location_id, status, date) 
                    VALUES (:location_id, :status, :date)");
            $stmt->bindValue(':location_id', $status->getStatusMessageLocationId());
            $stmt->bindValue(':status', $status->getStatusMessageState());
            $stmt->bindValue(':date', $status->getStatusMessageDate());
            $stmt->execute();

            if ($stmt) {
                $lastId = $this->connection->lastInsertId();
                return new StatusMessage($lastId,
                    $status->getStatusMessageLocationId(),
                    $status->getStatusMessageState(),
                    $status->getStatusMessageDate());
            }
            return null;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleteStatusById($id)
    {
        // TODO: Implement deleteStatusById() method.
    }

    public function updateStatusById($id)
    {
        // TODO: Implement updateStatusById() method.
    }

    public function findStatusMessageByLocationId($id) {
        try {
            $stmt = $this->connection->prepare("SELECT * FROM status_message WHERE location_id = :id");
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
            $stmt->execute();
            $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if (count($results) <= 0) {
                return null;
            }

            $status = [];

            foreach ($results as $s) {
                $status[] = $this->createNewStatusMessageFromResults($s);
            }

            return $status;
        }
        catch (\Exception $e) {
            return null;
        }
    }
}
