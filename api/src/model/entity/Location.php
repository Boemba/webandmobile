<?php

namespace api\model\entity;

/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 28/09/2017
 * Time: 22:01
 */
class Location
{

    private $locationId;
    private $locationName;

    /**
     * Location constructor.
     * @param $locationId
     * @param $locationName
     */
    public function __construct($locationId, $locationName)
    {
        $this->locationId = $locationId;
        $this->locationName = $locationName;
    }

    /**
     * @return mixed
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param mixed $locationId
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }

    /**
     * @return mixed
     */
    public function getLocationName()
    {
        return $this->locationName;
    }

    /**
     * @param mixed $locationName
     */
    public function setLocationName($locationName)
    {
        $this->locationName = $locationName;
    }
}