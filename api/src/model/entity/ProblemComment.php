<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 30/09/2017
 * Time: 17:31
 */

namespace api\model\entity;


class ProblemComment
{

    private $id;
    private $problemMessageId;
    private $problemCommentText;
    private $problemCommentDate;

    /**
     * ProblemComment constructor.
     * @param $id
     * @param $problemMessageId
     * @param $problemCommentText
     * @param $problemCommentDate
     */
    public function __construct($id, $problemMessageId, $problemCommentText, $problemCommentDate)
    {
        $this->id = $id;
        $this->problemMessageId = $problemMessageId;
        $this->problemCommentText = $problemCommentText;
        $this->problemCommentDate = $problemCommentDate;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProblemMessageId()
    {
        return $this->problemMessageId;
    }

    /**
     * @param mixed $problemMessageId
     */
    public function setProblemMessageId($problemMessageId)
    {
        $this->problemMessageId = $problemMessageId;
    }

    /**
     * @return mixed
     */
    public function getProblemCommentText()
    {
        return $this->problemCommentText;
    }

    /**
     * @param mixed $problemCommentText
     */
    public function setProblemCommentText($problemCommentText)
    {
        $this->problemCommentText = $problemCommentText;
    }

    /**
     * @return mixed
     */
    public function getProblemCommentDate()
    {
        return $this->problemCommentDate;
    }

    /**
     * @param mixed $problemCommentDate
     */
    public function setProblemCommentDate($problemCommentDate)
    {
        $this->problemCommentDate = $problemCommentDate;
    }
}