<?php
/**
 * Created by PhpStorm.
 * User: Alessio Marzo
 * Date: 15/10/2017
 * Time: 14:01
 */

namespace api\model\entity;


class ProblemUrgency
{

    private $id;
    private $problemId;
    private $urgencyLevel;

    /**
     * ProblemUrgency constructor.
     * @param $id
     * @param $problemId
     * @param $urgencyLevel
     */
    public function __construct($id, $problemId, $urgencyLevel)
    {
        $this->id = $id;
        $this->problemId = $problemId;
        $this->urgencyLevel = $urgencyLevel;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProblemId()
    {
        return $this->problemId;
    }

    /**
     * @param mixed $problemId
     */
    public function setProblemId($problemId)
    {
        $this->problemId = $problemId;
    }

    /**
     * @return mixed
     */
    public function getUrgencyLevel()
    {
        return $this->urgencyLevel;
    }

    /**
     * @param mixed $urgencyLevel
     */
    public function setUrgencyLevel($urgencyLevel)
    {
        $this->urgencyLevel = $urgencyLevel;
    }
}