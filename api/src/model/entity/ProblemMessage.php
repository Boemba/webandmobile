<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 28/09/2017
 * Time: 22:19
 */

namespace api\model\entity;


class ProblemMessage
{

    private $problemMessageId;
    private $problemMessageLocationId;
    private $problemMessageText;
    private $problemMessageDate;
    private $problemMessageIsFinished;

    /**
     * ProblemMessage constructor.
     * @param $problemMessageId
     * @param $problemMessageLocationId
     * @param $problemMessageText
     * @param $problemMessageDate
     * @param $problemMessageIsFinished
     */
    public function __construct($problemMessageId = null, $problemMessageLocationId = null, $problemMessageText = null, $problemMessageDate = null, $problemMessageIsFinished = null)
    {
        $this->problemMessageId = $problemMessageId;
        $this->problemMessageLocationId = $problemMessageLocationId;
        $this->problemMessageText = $problemMessageText;
        $this->problemMessageDate = $problemMessageDate;
        $this->problemMessageIsFinished = $problemMessageIsFinished;
    }

    /**
     * @return mixed
     */
    public function getProblemMessageId()
    {
        return $this->problemMessageId;
    }

    /**
     * @param mixed $problemMessageId
     */
    public function setProblemMessageId($problemMessageId)
    {
        $this->problemMessageId = $problemMessageId;
    }

    /**
     * @return mixed
     */
    public function getProblemMessageLocationId()
    {
        return $this->problemMessageLocationId;
    }

    /**
     * @param mixed $problemMessageLocationId
     */
    public function setProblemMessageLocationId($problemMessageLocationId)
    {
        $this->problemMessageLocationId = $problemMessageLocationId;
    }

    /**
     * @return mixed
     */
    public function getProblemMessageText()
    {
        return $this->problemMessageText;
    }

    /**
     * @param mixed $problemMessageText
     */
    public function setProblemMessageText($problemMessageText)
    {
        $this->problemMessageText = $problemMessageText;
    }

    /**
     * @return mixed
     */
    public function getProblemMessageDate()
    {
        return $this->problemMessageDate;
    }

    /**
     * @param mixed $problemMessageDate
     */
    public function setProblemMessageDate($problemMessageDate)
    {
        $this->problemMessageDate = $problemMessageDate;
    }

    /**
     * @return mixed
     */
    public function getProblemMessageIsFinished()
    {
        return $this->problemMessageIsFinished;
    }

    /**
     * @param mixed $problemMessageIsFinished
     */
    public function setProblemMessageIsFinished($problemMessageIsFinished)
    {
        $this->problemMessageIsFinished = $problemMessageIsFinished;
    }
}