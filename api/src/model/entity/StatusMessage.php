<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 28/09/2017
 * Time: 22:17
 */

namespace api\model\entity;


class StatusMessage
{

    private $statusMessageId;
    private $statusMessageLocationId;
    private $statusMessageState;
    private $statusMessageDate;

    /**
     * StatusMessage constructor.
     * @param $statusMessageId
     * @param $statusMessageLocationId
     * @param $statusMessageState
     * @param $statusMessageDate
     */
    public function __construct($statusMessageId = null, $statusMessageLocationId = null, $statusMessageState = null, $statusMessageDate = null)
    {
        $this->statusMessageId = $statusMessageId;
        $this->statusMessageLocationId = $statusMessageLocationId;
        $this->statusMessageState = $statusMessageState;
        $this->statusMessageDate = $statusMessageDate;
    }

    /**
     * @return mixed
     */
    public function getStatusMessageId()
    {
        return $this->statusMessageId;
    }

    /**
     * @param mixed $statusMessageId
     */
    public function setStatusMessageId($statusMessageId)
    {
        $this->statusMessageId = $statusMessageId;
    }

    /**
     * @return mixed
     */
    public function getStatusMessageLocationId()
    {
        return $this->statusMessageLocationId;
    }

    /**
     * @param mixed $statusMessageLocationId
     */
    public function setStatusMessageLocationId($statusMessageLocationId)
    {
        $this->statusMessageLocationId = $statusMessageLocationId;
    }

    /**
     * @return mixed
     */
    public function getStatusMessageState()
    {
        return $this->statusMessageState;
    }

    /**
     * @param mixed $statusMessageState
     */
    public function setStatusMessageState($statusMessageState)
    {
        $this->statusMessageState = $statusMessageState;
    }

    /**
     * @return mixed
     */
    public function getStatusMessageDate()
    {
        return $this->statusMessageDate;
    }

    /**
     * @param mixed $statusMessageDate
     */
    public function setStatusMessageDate($statusMessageDate)
    {
        $this->statusMessageDate = $statusMessageDate;
    }
}