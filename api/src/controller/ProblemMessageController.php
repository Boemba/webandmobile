<?php

namespace api\controller;

use api\model\entity\ProblemMessage;
use api\model\entity\ProblemComment;
use api\model\repository\ProblemMessageRepository;
use api\view\View;

class ProblemMessageController
{

    private $problemMessageRepository;
    private $problemMessageView;

    public function __construct(ProblemMessageRepository $problemMessageRepository, View $view)
    {
        $this->problemMessageView = $view;
        $this->problemMessageRepository = $problemMessageRepository;
    }

    public function handleFindProblemById($id = null)
    {
        if ($id == null) {
            return;
        }

        $problem = $this->problemMessageRepository->findProblemMessageById($id);

        $this->problemMessageView->show(array('problem' => $problem));
    }

    public function handleFindProblemByLocationId($id = null)
    {
        if ($id == null) {
            return;
        }

        $problems = $this->problemMessageRepository->findProblemMessageByLocationId($id);

        $this->problemMessageView->show(array('problems' => $problems));
    }

    public function handleFindAllProblems()
    {
        $problems = $this->problemMessageRepository->findAllProblems();

        $this->problemMessageView->show(array('problems' => $problems));
    }

    public function handleInsertProblemMessage(ProblemMessage $problemMessage = null)
    {
        $problemMessage = new ProblemMessage(NULL,
            $problemMessage->getProblemMessageLocationId(),
            $problemMessage->getProblemMessageText(),
            $problemMessage->getProblemMessageDate(),
            $problemMessage->getProblemMessageIsFinished());

        $problemMessage = $this->problemMessageRepository->insertProblemMessage($problemMessage);

        $this->problemMessageView->show(array('problem' => $problemMessage));
    }

    public function getProblemWithId($id)
    {
        if ($id == null) {
            return null;
        }

        $problem = $this->problemMessageRepository->findProblemMessageById($id);

        return $problem;
    }

}
