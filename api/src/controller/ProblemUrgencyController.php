<?php

namespace api\controller;

use api\model\entity\ProblemUrgency;
use api\model\repository\ProblemUrgencyRepository;
use api\view\View;

class ProblemUrgencyController
{
    private $problemUrgencyRepository;
    private $problemUrgencyView;

    /**
     * ProblemUrgencyController constructor.
     * @param $problemUrgencyRepository
     * @param $problemUrgencyView
     */
    public function __construct(ProblemUrgencyRepository $problemUrgencyRepository,View $problemUrgencyView)
    {
        $this->problemUrgencyRepository = $problemUrgencyRepository;
        $this->problemUrgencyView = $problemUrgencyView;
    }

    public function handleFindProblemUrgencyByProblemId($id, $problem)
    {
        if($id == null) {
            return;
        }

        $problemUrgency = $this->problemUrgencyRepository->findProblemUrgencyByProblemId($id);

        $this->problemUrgencyView->show(array('problem' => $problem, 'problemUrgency' => $problemUrgency));
    }

    public function handleInsertProblemUrgency(ProblemUrgency $problemUrgency = null)
    {
        $problemUrgency = new ProblemUrgency(null, $problemUrgency->getProblemId(),
            $problemUrgency->getUrgencyLevel());

        $problemUrgency = $this->problemUrgencyRepository->insertProblemUrgency($problemUrgency);

        $this->problemUrgencyView->show(array('problemUrgency' => $problemUrgency));
    }

    public function handleUpdateProblemUrgency(ProblemUrgency $problemUrgency = null)
    {
        $problemUrgency = new ProblemUrgency(null, $problemUrgency->getProblemId(),
            $problemUrgency->getUrgencyLevel());

        $problemUrgency = $this->problemUrgencyRepository->updateProblemUrgency($problemUrgency);

        $this->problemUrgencyView->show(array('problemUrgency' => $problemUrgency));
    }
}