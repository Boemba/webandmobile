<?php
/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 30/09/2017
 * Time: 18:08
 */

namespace api\controller;


use api\model\entity\ProblemComment;
use api\model\repository\ProblemCommentRepository;
use api\view\View;

class ProblemCommentController
{

    private $problemCommentRepository;
    private $problemCommentView;

    /**
     * ProblemCommentController constructor.
     * @param $problemCommentRepository
     * @param $problemCommentView
     */
    public function __construct(ProblemCommentRepository $problemCommentRepository, View $view)
    {
        $this->problemCommentRepository = $problemCommentRepository;
        $this->problemCommentView = $view;
    }


    public function handleFindProblemCommentsById($id, $problem)
    {
        if($id == null) {
            return;
        }

        $comments = $this->problemCommentRepository->findProblemMessageCommentsByProblemId($id);

        $this->problemCommentView->show(array('problem' => $problem, 'comments' => $comments));
    }

    public function handleInsertProblemComment(ProblemComment $problemComment)
    {
        $problemComment = $this->problemCommentRepository->insertProblemComment($problemComment);

        $this->problemCommentView->show(array('comment' => $problemComment));
    }
}