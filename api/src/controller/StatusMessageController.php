<?php

namespace api\controller;

use api\model\entity\StatusMessage;
use api\model\repository\StatusMessageRepository;
use api\view\View;

class StatusMessageController {

    private $statusMessageRepository;
    private $statusMessageView;

    public function __construct(StatusMessageRepository $statusMessageRepository, View $view) {
        $this->statusMessageView = $view;
        $this->statusMessageRepository = $statusMessageRepository;
    }

    public function handleFindStatusById($id = null)
    {
        if($id == null) {
            return;
        }

        $status = $this->statusMessageRepository->findStatusMessageById($id);

        $this->statusMessageView->show(array('status' => $status));
    }

    public function handleInsertStatus(StatusMessage $status = null) {
        $status = new StatusMessage(NULL, $status->getStatusMessageLocationId(), $status->getStatusMessageState(), $status->getStatusMessageDate());

        $status = $this->statusMessageRepository->insertStatusMessage($status);

        $this->statusMessageView->show(array('status' => $status));
    }

    public function handleFindStatusByLocationId($id = null)
    {
        if($id == null) {
            return;
        }

        $statuses = $this->statusMessageRepository->findStatusMessageByLocationId($id);

        $this->statusMessageView->show(array('statuses' => $statuses));
    }
}