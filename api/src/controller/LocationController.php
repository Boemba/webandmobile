<?php

namespace api\controller;

use api\model\repository\PDOLocationRepository;
use api\view\View;
use api\model\repository\LocationRepository;

class LocationController {

    private $locationRepository;
    private $locationView;

    public function __construct(LocationRepository $locationRepository, View $view)
    {
        $this->locationRepository = $locationRepository;
        $this->locationView = $view;
    }

    public function handleFindAllLocations()
    {
        $locations = $this->locationRepository->findAllLocations();

        if($locations == null) {
            return;
        }

        $this->locationView->show(array('locations' => $locations));
    }

    public function handleFindLocationById($id = null)
    {
        if($id == null) {
            return;
        }

        $location = $this->locationRepository->findLocationById($id);

        $this->locationView->show(array('location' => $location));
    }
}
