<?php

/**
 * Created by PhpStorm.
 * User: Ebert Joris
 * Date: 14/10/2017
 * Time: 12:44
 */

require '../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

class AppTest extends TestCase {
    //const IP = 'http://192.168.33.22/';
    const IP = 'http://localhost:8079/webandmobile/';

    /**
     * @var Client|$client
     */
    private $client;

    protected function setUp()
    {
        $this->client = new Client();
    }

    protected function tearDown()
    {
        $this->client = null;
    }

    public function testGetLocation_locationExists_Status200LocationObject() {
        //Arrange
        $id = 1;
        $name = "PXL - BE041";

        //Act
        $response = $this->client->get(self::IP . "api/api.php/location/$id");
        $statuscode = $response->getStatusCode();
        $body = $response->getBody();
        $jsonArray = json_decode($body, true);

        //Assert
        $this->assertEquals('200', $statuscode);
        $this->assertEquals($id, $jsonArray['id']);
        $this->assertEquals($name, $jsonArray['name']);
    }

    public function testGetProblem_problemExists_Status200ProblemObject() {
        //Arrange
        $id = 1;
        $date = "2017-10-18 06:23:20";

        //Act
        $response = $this->client->get(self::IP . "api/api.php/problem/$id");
        $statuscode = $response->getStatusCode();
        $body = $response->getBody();
        $jsonArray = json_decode($body, true);

        //Assert
        $this->assertEquals('200', $statuscode);
        $this->assertEquals($id, $jsonArray['problemId']);
        $this->assertEquals($date, $jsonArray['problemDate']);
    }

    public function testGetStats_statusExists_Status200StatusObject() {
        //Arrange
        $id = "1";
        $date = "2017-09-13 05:27:42";

        //Act
        $response = $this->client->get(self::IP . "api/api.php/status/$id");
        $statuscode = $response->getStatusCode();
        $body = $response->getBody();
        $jsonArray = json_decode($body, true);

        //Assert
        $this->assertEquals('200', $statuscode);
        $this->assertEquals($id, $jsonArray['statusId']);
        $this->assertEquals($date, $jsonArray['statusDate']);
    }

   public function testGetProblemComments_commentsExists_Status200ProblemWithCommentsObject() {
        //Arrange
        $problemId = "1";
        $problemDate = "2017-10-18 06:23:20";
        $problemFirstCommentId = "2"; //They are ordered with on date, don't get tricked by their id's.
        $problemFirstCommentDate = "2017-10-10";


        //Act
        $response = $this->client->get(self::IP . "api/api.php/problem/1/comments/");
        $statuscode = $response->getStatusCode();
        $body = $response->getBody();
        $jsonArray = json_decode($body, true);

        //var_dump($jsonArray);

        //Assert
        $this->assertEquals('200', $statuscode);
        $this->assertEquals($problemId, $jsonArray['problemId']);
        $this->assertEquals($problemDate, $jsonArray['problemDate']);
        $this->assertEquals($problemFirstCommentId, $jsonArray['problemComment'][0]['id']);
        $this->assertEquals($problemFirstCommentDate, $jsonArray['problemComment'][0]['problemDate']);
    }
}