<?php

require_once '../../src/model/entity/Location.php';
require_once '../../src/model/Repository/LocationRepository.php';
require_once '../../src/model/Repository/PDOLocationRepository.php';
require_once '../../src/view/View.php';
require_once '../../src/view/LocationJsonView.php';
require_once '../../src/controller/LocationController.php';

use \api\model\entity\Location;
use \api\model\repository\LocationRepository;
use \api\model\repository\PDOLocationRepository;
use \api\controller\LocationController;
use \api\view\View;
use \api\view\LocationJsonView;

class LocationControllerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var LocationRepository|PHPUnit_Framework_MockObject_MockObject $mockLocationRepository
     */
    private $mockLocationRepository;

    /**
     * @var View|PHPUnit_Framework_MockObject_MockObject $mockView
     */
    private $mockView;

    /**
     * @var Location $location
     */
    private $location;

    public function setUp()
    {
        $this->mockLocationRepository = $this->getMockBuilder('\api\model\repository\LocationRepository')->getMock();
        $this->mockView = $this->getMockBuilder('\api\view\View')->getMock();
        $this->location = new Location(123, 'BE-123');
    }

    public function tearDown()
    {
        $this->mockLocationRepository = null;
        $this->mockView = null;
        $this->location = null;
    }

    public function testHandleFindAllLocationsAreFound()
    {
        $this->mockLocationRepository->expects($this->once())
            ->method('findAllLocations')
            ->will($this->returnValue($this->location));

        $this->mockView->expects($this->once())
            ->method('show')
            ->with($this->equalTo(['locations' => $this->location]))
            ->will($this->returnCallback(function ($object) {
                /** @var Location $l */
                $l = $object['locations'];
                $this->assertEquals($this->location, $l);
            }));

        $locationController = new LocationController($this->mockLocationRepository, $this->mockView);
        $locationController->handleFindAllLocations();
    }

    public function testHandleFindLocationWithIdIsFound()
    {
        $this->mockLocationRepository->expects($this->once())
            ->method('findLocationById')
            ->will($this->returnValue($this->location));

        $this->mockView->expects($this->once())
            ->method('show')
            ->with($this->equalTo(['location' => $this->location]))
            ->will($this->returnCallback(function ($object) {
                /** @var Location $l */
                $l = $object['location'];
                $this->assertEquals($this->location, $l);
            }));

        $locationController = new LocationController($this->mockLocationRepository, $this->mockView);
        $locationController->handleFindLocationById($this->location->getLocationId());
    }

    public function testHandleFindLocationWithIdIsNotFound()
    {
        $wrongId = 222;

        $this->mockLocationRepository->expects($this->once())
            ->method('findLocationById')
            ->with($this->equalTo($wrongId))
            ->will($this->returnValue(null));

        $this->mockView->expects($this->once())
            ->method('show')
            ->with($this->equalTo(['location' => null]))
            ->will($this->returnCallback(function ($object) {
                /** @var Location $l */
                $l = $object['location'];
                $this->assertNull($l);
            }));

        $LocationController = new LocationController($this->mockLocationRepository, $this->mockView);
        $LocationController->handleFindLocationById($wrongId);
    }
}
