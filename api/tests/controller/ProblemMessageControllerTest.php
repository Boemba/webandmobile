<?php

require_once '../../src/model/Entity/ProblemMessage.php';
require_once '../../src/model/Repository/ProblemMessageRepository.php';
require_once '../../src/model/Repository/PDOProblemMessageRepository.php';
require_once '../../src/view/View.php';
require_once '../../src/view/ProblemMessageJsonView.php';
require_once '../../src/controller/ProblemMessageController.php';

use \api\model\entity\ProblemMessage;
use \api\model\repository\ProblemMessageRepository;
use \api\model\repository\PDOProblemMessageRepository;
use \api\controller\ProblemMessageController;
use \api\view\View;
use \api\view\ProblemMessageJsonView;

class ProblemMessageControllerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ProblemMessageRepository|PHPUnit_Framework_MockObject_MockObject $mockProblemMessageRepository
     */
    private $mockProblemMessageRepository;

    /**
     * @var View|PHPUnit_Framework_MockObject_MockObject $mockView
     */
    private $mockView;

    /**
     * @var ProblemMessage $problemMessage
     */
    private $problemMessage;

    public function setUp()
    {
        $this->mockProblemMessageRepository = $this->getMockBuilder('\api\model\repository\ProblemMessageRepository')->getMock();
        $this->mockView = $this->getMockBuilder('\api\view\View')->getMock();
        $this->problemMessage = new ProblemMessage(1, 1, 'test', null, null);
    }

    public function tearDown()
    {
        $this->mockProblemMessageRepository = null;
        $this->mockView = null;
        $this->problemMessage = null;
    }

    public function testHandleFindAllProblemMessageAreFound()
    {
        $this->mockProblemMessageRepository->expects($this->once())
            ->method('FindAllProblems')
            ->will($this->returnValue($this->problemMessage));

        $this->mockView->expects($this->once())
            ->method('show')
            ->with($this->equalTo(['problems' => $this->problemMessage]))
            ->will($this->returnCallback(function ($object) {
                /** @var ProblemMessage $p */
                $p = $object['problems'];
                $this->assertEquals($this->problemMessage, $p);
            }));

        $problemMessageController = new ProblemMessageController($this->mockProblemMessageRepository, $this->mockView);
        $problemMessageController->handleFindAllProblems();
    }

    public function testHandleFindProblemMessageWithIdIsFound()
    {
        $this->mockProblemMessageRepository->expects($this->once())
            ->method('findProblemMessageById')
            ->will($this->returnValue($this->problemMessage));

        $this->mockView->expects($this->once())
            ->method('show')
            ->with($this->equalTo(['problem' => $this->problemMessage]))
            ->will($this->returnCallback(function ($object) {
                /** @var ProblemMessage $p */
                $p = $object['problem'];
                $this->assertEquals($this->problemMessage, $p);
            }));

        $problemMessageController = new ProblemMessageController($this->mockProblemMessageRepository, $this->mockView);
        $problemMessageController->handleFindProblemById($this->problemMessage->getProblemMessageId());
    }

    public function testHandleFindProblemMessageWithIdIsNotFound()
    {
        $wrongId = 2;

        $this->mockProblemMessageRepository->expects($this->once())
            ->method('findProblemMessageById')
            ->will($this->returnValue(null));

        $this->mockView->expects($this->once())
            ->method('show')
            ->with($this->equalTo(['problem' => null]))
            ->will($this->returnCallback(function ($object) {
                /** @var ProblemMessage $p */
                $p = $object['problem'];
                $this->assertNull($p);
            }));

        $problemMessageController = new ProblemMessageController($this->mockProblemMessageRepository, $this->mockView);
        $problemMessageController->handleFindProblemById($wrongId);
    }

    public function testHandleInsertProblemMessageCompleted()
    {
        $this->mockProblemMessageRepository->expects($this->once())
            ->method('insertProblemMessage')
            ->will($this->returnValue($this->problemMessage));

        $this->mockView->expects($this->once())
            ->method('show')
            ->with($this->equalTo(['problem' => $this->problemMessage]))
            ->will($this->returnCallback(function ($object) {
                /** @var ProblemMessage $p */
                $p = $object['problem'];
                $this->assertNotNull($p);
            }));

        $problemMessageController = new ProblemMessageController($this->mockProblemMessageRepository, $this->mockView);
        $problemMessageController->handleInsertProblemMessage($this->problemMessage);
    }

    public function testHandleDeleteProblemMessageByIdCompleted()
    {

    }

    public function testHandleUpdateProblemMessageByIdCompleted()
    {

    }
}