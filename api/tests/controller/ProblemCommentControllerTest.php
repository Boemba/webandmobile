<?php

require_once '../../src/model/Entity/ProblemComment.php';
require_once '../../src/model/Entity/ProblemMessage.php';
require_once '../../src/model/Repository/ProblemCommentRepository.php';
require_once '../../src/model/Repository/PDOProblemCommentRepository.php';
require_once '../../src/view/View.php';
require_once '../../src/view/ProblemCommentJsonView.php';
require_once '../../src/controller/ProblemCommentController.php';

use \api\model\entity\ProblemComment;
use \api\model\entity\ProblemMessage;
use \api\model\repository\ProblemCommentRepository;
use \api\model\repository\PDOProblemCommentRepository;
use \api\controller\ProblemCommentController;
use \api\view\View;
use \api\view\ProblemCommentJsonView;

class ProblemCommentControllerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var ProblemCommentRepository|PHPUnit_Framework_MockObject_MockObject $mockProblemCommentRepository
     */
    private $mockProblemCommentRepository;

    /**
     * @var View|PHPUnit_Framework_MockObject_MockObject $mockView
     */
    private $mockView;

    /**
     * @var problemComment $problemComment
     */
    private $problemComment;

    /**
     * @var ProblemMessage $problemMessage
     */
    private $problemMessage;

    public function setUp()
    {
        $this->mockProblemCommentRepository = $this->getMockBuilder('\api\model\repository\ProblemCommentRepository')->getMock();
        $this->mockView = $this->getMockBuilder('\api\view\View')->getMock();
        $this->problemComment = new ProblemComment(1, 1, "Comment example text", "29-08-2017");
        $this->problemMessage = new ProblemMessage(1, 1, "Problem example text", "29-06-2017", false);
    }

    public function tearDown()
    {
        $this->mockProblemCommentRepository = null;
        $this->mockView = null;
        $this->problemComment = null;
    }

    public function testHandleFindProblemCommentWithIdIsFound()
    {
        $this->mockProblemCommentRepository->expects($this->once())
            ->method('findProblemMessageCommentsByProblemId')
            ->will($this->returnValue($this->problemComment));

        $this->mockView->expects($this->once())
            ->method('show')
            ->with($this->equalTo(['problem' => $this->problemMessage, 'comments' => $this->problemComment]))
            ->will($this->returnCallback(function ($object) {
                /** @var ProblemComment $c */
                $c = $object['comments'];
                echo $c->getId() . ' ' . $c->getProblemMessageId() . ' ' . $c->getProblemCommentText() . ' ' . $c->getProblemCommentDate();
            }));

        $problemCommentController = new ProblemCommentController($this->mockProblemCommentRepository, $this->mockView);
        $problemCommentController->handleFindProblemCommentsById($this->problemComment->getProblemMessageId(), $this->problemMessage);
    }

    public function testHandleFindProblemCommentWithIdIsNotFound()
    {
        $wrongId = 2;

        $this->mockProblemCommentRepository->expects($this->once())
            ->method('findProblemMessageCommentsByProblemId')
            ->will($this->returnValue(null));

        $this->mockView->expects($this->once())
            ->method('show')
            ->with($this->equalTo(['problem' => null, 'comments' => null]))
            ->will($this->returnCallback(function ($object) {
                /** @var ProblemComment $p */
                $c = $object['comments'];
                $this->assertNull($c);
            }));

        $problemCommentController = new ProblemCommentController($this->mockProblemCommentRepository, $this->mockView);
        $problemCommentController->handleFindProblemCommentsById($wrongId, null);
    }
}
