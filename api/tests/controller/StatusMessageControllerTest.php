<?php

require_once '../../src/model/Entity/StatusMessage.php';
require_once '../../src/model/Repository/StatusMessageRepository.php';
require_once '../../src/model/Repository/PDOStatusMessageRepository.php';
require_once '../../src/view/View.php';
require_once '../../src/view/StatusMessageJsonView.php';
require_once '../../src/controller/StatusMessageController.php';

use \api\model\entity\StatusMessage;
use \api\model\repository\StatusMessageRepository;
use \api\model\repository\PDOStatusMessageRepository;
use \api\controller\StatusMessageController;
use \api\view\View;
use \api\view\StatusMessageJsonView;

class StatusMessageControllerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var StatusMessageRepository|PHPUnit_Framework_MockObject_MockObject $mockStatusMessageRepository
     */
    private $mockStatusMessageRepository;

    /**
     * @var View|PHPUnit_Framework_MockObject_MockObject $mockView
     */
    private $mockView;

    /**
     * @var StatusMessage $statusMessage
     */
    private $statusMessage;

    public function setUp()
    {
        $this->mockStatusMessageRepository = $this->getMockBuilder('\api\model\repository\StatusMessageRepository')->getMock();
        $this->mockView = $this->getMockBuilder('\api\view\View')->getMock();
        $this->statusMessage = new StatusMessage(1, 1, 'test', '2017-01-01');
    }

    public function tearDown()
    {
        $this->mockStatusMessageRepository = null;
        $this->mockView = null;
        $this->statusMessage = null;
    }

    public function testHandleFindStatusMessageWithIdIsFound()
    {
        $this->mockStatusMessageRepository->expects($this->once())
            ->method('findStatusMessageById')
            ->will($this->returnValue($this->statusMessage));

        $this->mockView->expects($this->once())
            ->method('show')
            ->with($this->equalTo(['status' => $this->statusMessage]))
            ->will($this->returnCallback(function ($object) {
                /** @var StatusMessage $p */
                $s = $object['status'];
                $this->assertEquals($this->statusMessage, $s);
            }));

        $statusMessageController = new StatusMessageController($this->mockStatusMessageRepository, $this->mockView);
        $statusMessageController->handleFindStatusById($this->statusMessage->getStatusMessageId());
    }

    public function testHandleFindStatusMessageWithIdIsNotFound()
    {
        $wrongId = 2;

        $this->mockStatusMessageRepository->expects($this->once())
            ->method('findStatusMessageById')
            ->will($this->returnValue(null));

        $this->mockView->expects($this->once())
            ->method('show')
            ->with($this->equalTo(['status' => null]))
            ->will($this->returnCallback(function ($object) {
                /** @var StatusMessage $p */
                $s = $object['status'];
                $this->assertNull($s);
            }));

        $statusMessageController = new StatusMessageController($this->mockStatusMessageRepository, $this->mockView);
        $statusMessageController->handleFindStatusById($wrongId);
    }

    public function testHandleInsertStatusMessageCompleted()
    {
        $this->mockStatusMessageRepository->expects($this->once())
            ->method('insertStatusMessage')
            ->will($this->returnValue($this->statusMessage));

        $this->mockView->expects($this->once())
            ->method('show')
            ->with($this->equalTo(['status' => $this->statusMessage]))
            ->will($this->returnCallback(function ($object) {
                /** @var StatusMessage $p */
                $p = $object['status'];
                $this->assertNotNull($p);
            }));

        $statusMessageController = new StatusMessageController($this->mockStatusMessageRepository, $this->mockView);
        $statusMessageController->handleInsertStatus($this->statusMessage);
    }

    public function testHandleDeleteStatusMessageByIdCompleted()
    {

    }

    public function testHandleUpdateStatusMessageByIdCompleted()
    {

    }
}
