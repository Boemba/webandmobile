<?php

require_once '../../src/model/Entity/ProblemMessage.php';
require_once '../../src/model/Repository/ProblemMessageRepository.php';
require_once '../../src/model/Repository/PDOProblemMessageRepository.php';
require_once '../../src/view/View.php';
require_once '../../src/view/ProblemMessageJsonView.php';
require_once '../../src/controller/ProblemMessageController.php';

use \api\model\entity\ProblemMessage;
use \api\model\repository\ProblemMessageRepository;
use \api\model\repository\PDOProblemMessageRepository;
use \api\controller\ProblemMessageController;
use \api\view\View;
use \api\view\ProblemMessageJsonView;

class PDOProblemMessageRepositoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var PDO $connection
     */
    private $connection;

    public function setUp()
    {
        $this->connection = new PDO('sqlite::memory:');
        $this->connection->exec('CREATE TABLE problem_message (id INT, location_id INT, problem VARCHAR(255), date DATE, solved VARCHAR(255), PRIMARY KEY (id))');
    }

    public function tearDown()
    {
        $this->connection = null;
    }

    public function testHandleFindAllProblemMessages()
    {
        $id = 1;
        $locationId = 1;
        $problem = 'De projector is kapot';
        $date = null;
        $solved = 'Solved';
        $problemMessage = new ProblemMessage($id, $locationId, $problem, $date, $solved);

        $this->connection->exec("INSERT INTO problem_message(id, location_id, problem, date, solved) VALUES ($id, $locationId, '$problem', '$date', '$solved')");
        $PDOProblemMessageRepository = new PDOProblemMessageRepository($this->connection);

        $actualProblemMessage = $PDOProblemMessageRepository->findAllProblems();

        $this->assertEquals([$problemMessage], $actualProblemMessage);
    }

    public function testHandleFindProblemMessageByIdFound()
    {
        $id = 1;
        $locationId = 101;
        $problem = 'De projector is kapot';
        $date = null;
        $solved = 'Solved';
        $problemMessage = new ProblemMessage($id, $locationId, $problem, $date, $solved);

        $this->connection->exec("INSERT INTO problem_message(id, location_id, problem, date, solved) VALUES ($id, $locationId, '$problem', '$date', '$solved')");
        $PDOProblemMessageRepository = new PDOProblemMessageRepository($this->connection);

        $actualProblemMessage = $PDOProblemMessageRepository->findProblemMessageById(1);

        $this->assertEquals($problemMessage, $actualProblemMessage);
    }

    public function testHandleFindProblemMessageByIdNotFound()
    {
        $id = 1;
        $locationId = 101;
        $problem = 'De projector is kapot';
        $date = null;
        $solved = 'Solved';

        $this->connection->exec("INSERT INTO problem_message(id, location_id, problem, date, solved) VALUES ($id, $locationId, '$problem', '$date', '$solved')");
        $PDOProblemMessageRepository = new PDOProblemMessageRepository($this->connection);

        $actualProblemMessage = $PDOProblemMessageRepository->findProblemMessageById(2);

        $this->assertNull($actualProblemMessage);
    }

    public function testHandleInsertProblemMessageSucceeds()
    {
        $id = null;
        $locationId = 101;
        $problem = 'De projector is kapot';
        $date = '24-09-2017';
        $solved = 'Solved';
        $problemMessage = new ProblemMessage($id, $locationId, $problem, $date, $solved);

        $PDOProblemMessageRepository = new PDOProblemMessageRepository($this->connection);

        $insertedProblemMessage = $PDOProblemMessageRepository->insertProblemMessage($problemMessage);

        $this->assertEquals($problemMessage->getProblemMessageText(), $insertedProblemMessage->getProblemMessageText());
    }
}
