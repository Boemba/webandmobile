<?php

require_once '../../src/model/Entity/ProblemComment.php';
require_once '../../src/model/Repository/ProblemCommentRepository.php';
require_once '../../src/model/Repository/PDOProblemCommentRepository.php';
require_once '../../src/view/View.php';
require_once '../../src/view/ProblemCommentJsonView.php';
require_once '../../src/controller/ProblemCommentController.php';

use \api\model\entity\ProblemComment;
use \api\model\repository\ProblemCommentRepository;
use \api\model\repository\PDOProblemCommentRepository;
use \api\controller\ProblemCommentController;
use \api\view\View;
use \api\view\ProblemCommentJsonView;

class PDOProblemCommentRepositoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var PDO $connection
     */
    private $connection;

    public function setUp()
    {
        $this->connection = new PDO('sqlite::memory:');
        $this->connection->exec('CREATE TABLE problem_comment (id INT, problem_id INT, comment_text VARCHAR(255), comment_date DATE) PRIMARY KEY (id))');
    }

    public function tearDown()
    {
        $this->connection = null;
    }

    public function testHandleFindProblemMessageCommentsByProblemIdFound()
    {
        $id = 1;
        $problemId = 1;
        $commentText = 'Some Comment...';
        $commentDate = null;
        $problemComment = new ProblemComment($id, $problemId, $commentText, $commentDate);

        //$this->connection->exec("INSERT INTO problem_comment (id, problem_id, comment_text, comment_date) VALUES ($id, $problemId, $commentText, $commentDate);");
        //$PDOProblemCommentRepository = new PDOProblemCommentRepository($this->connection);

        //$actualProblemComment = $PDOProblemCommentRepository->findProblemMessageCommentsByProblemId($problemId);

        //$this->assertEquals($problemComment, $actualProblemComment);
    }
}
