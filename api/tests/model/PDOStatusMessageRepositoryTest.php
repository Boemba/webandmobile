<?php

require_once '../../src/model/Entity/StatusMessage.php';
require_once '../../src/model/Repository/StatusMessageRepository.php';
require_once '../../src/model/Repository/PDOStatusMessageRepository.php';
require_once '../../src/view/View.php';
require_once '../../src/view/StatusMessageJsonView.php';
require_once '../../src/controller/StatusMessageController.php';

use \api\model\entity\StatusMessage;
use \api\model\repository\StatusMessageRepository;
use \api\model\repository\PDOStatusMessageRepository;
use \api\controller\StatusMessageController;
use \api\view\View;
use \api\view\StatusMessageJsonView;


class PDOStatusMessageRepositoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var PDO $connection
     */
    private $connection;

    public function setUp()
    {
        $this->connection = new PDO('sqlite::memory:');
        $this->connection->exec('CREATE TABLE status_message (id INT, location_id INT, status VARCHAR(255), date DATE, PRIMARY KEY  (id))');
    }

    public function tearDown()
    {
        $this->connection = null;
    }

    public function testHandleFindStatusMessageByIdFound()
    {
        $id = 1;
        $locationId = 101;
        $status = 'Goed';
        $date = '2017-03-12';
        $statusMessage = new StatusMessage($id, $locationId, $status, $date);

        $this->connection->exec("INSERT INTO status_message (id, location_id, status, date) VALUES ($id, $locationId, '$status', '$date')");
        $PDOStatusMessageRepository = new PDOStatusMessageRepository($this->connection);

        $actualStatusMessage = $PDOStatusMessageRepository->findStatusMessageById($id);

        $this->assertEquals($statusMessage, $actualStatusMessage);
    }

    public function testHandleFindStatusMessageByIdFoundNotFound()
    {
        $id = 1;
        $locationId = 101;
        $status = 'Goed';
        $date = '2017-03-12';

        $this->connection->exec("INSERT INTO status_message (id, location_id, status, date) VALUES ($id, $locationId, '$status', '$date')");
        $PDOStatusMessageRepository = new PDOStatusMessageRepository($this->connection);

        $actualStatusMessage = $PDOStatusMessageRepository->findStatusMessageById(3);

        $this->assertNull($actualStatusMessage);
    }

    //TODO
    public function testHandleInsertStatusMessageSucceeds()
    {
        $id = null;
        $locationId = 101;
        $status = 'Goed';
        $date = '2017-03-12';
        $statusMessage = new StatusMessage($id, $locationId, $status, $date);

        $PDOStatusMessageRepository = new PDOStatusMessageRepository($this->connection);

        $insertedStatusMessage = $PDOStatusMessageRepository->insertStatusMessage($statusMessage);

        $this->assertEquals($statusMessage->getStatusMessageLocationId(), $insertedStatusMessage->getStatusMessageLocationId());
    }
}
