<?php

require_once '../../src/model/Entity/Location.php';
require_once '../../src/model/Repository/LocationRepository.php';
require_once '../../src/model/Repository/PDOLocationRepository.php';
require_once '../../src/view/View.php';
require_once '../../src/view/LocationJsonView.php';

use \api\model\entity\Location;
use \api\model\repository\LocationRepository;
use \api\model\repository\PDOLocationRepository;
use \api\controller\LocationController;
use \api\view\View;
use \api\view\LocationJsonView;

class PDOLocationRepositoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var PDO $connection
     */
    private $connection;

    public function setUp()
    {
        $this->connection = new PDO('sqlite::memory:');
        $this->connection->exec('CREATE TABLE location (id INT, name VARCHAR(255), PRIMARY KEY  (id))');
    }

    public function tearDown()
    {
        $this->connection = null;
    }

    public function testHandleFindAllLocationsAreFound()
    {
        $id = 1;
        $name = 'B151';
        $location = new Location($id, $name);

        $this->connection->exec("INSERT INTO location (id, name) VALUES ($id, '$name');");
        $PDOLocationRepository = new PDOLocationRepository($this->connection);

        $actualLocations = $PDOLocationRepository->findAllLocations();

        $this->assertEquals([$location], $actualLocations);
    }

    public function testHandleFindLocationByIdFound()
    {
        $id = 1;
        $name = 'B151';
        $location = new Location($id, $name);

        $this->connection->exec("INSERT INTO location (id, name) VALUES ($id, '$name');");
        $PDOLocationRepository = new PDOLocationRepository($this->connection);

        $actualLocation = $PDOLocationRepository->findLocationById($id);

        $this->assertEquals($location, $actualLocation);
    }

    public function testHandleFindLocationByIdNotFound()
    {
        $id = 1;
        $name = 'B151';

        $this->connection->exec("INSERT INTO location (id, name) VALUES ($id, '$name');");
        $PDOLocationRepository = new PDOLocationRepository($this->connection);

        $actualLocation = $PDOLocationRepository->findLocationById(2);

        $this->assertNull($actualLocation);
    }
}
