<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require './vendor/autoload.php';
require 'vendor/altorouter/altorouter/AltoRouter.php';

use api\controller\LocationController;
use \api\model\entity\Location;
use api\model\repository\PDOLocationRepository;
use api\view\LocationJsonView;

use api\controller\ProblemMessageController;
use \api\model\entity\ProblemMessage;
use api\model\repository\PDOProblemMessageRepository;
use api\view\ProblemMessageJsonView;

use api\controller\StatusMessageController;
use \api\model\entity\StatusMessage;
use api\model\repository\PDOStatusMessageRepository;
use api\view\StatusMessageJsonView;

use api\controller\ProblemCommentController;
use \api\model\entity\ProblemComment;
use api\model\repository\PDOProblemCommentRepository;
use api\view\ProblemCommentJsonView;

use api\controller\ProblemUrgencyController;
use \api\model\entity\ProblemUrgency;
use api\model\repository\PDOProblemUrgencyRepository;
use api\view\ProblemUrgencyJsonView;

$pdo = null;

try {
    $database = require_once '../api/src/config/database.php';
    $pdo = new PDO("mysql:host=". $database['host'] . ";dbname=" . $database['database'], $database['username'], $database['password']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
    http_response_code(500);
}

$locationPdoRepository = new PDOLocationRepository($pdo);
$locationJsonView = new LocationJsonView();
$locationController = new LocationController($locationPdoRepository, $locationJsonView);

$problemMessagePdoRepository = new PDOProblemMessageRepository($pdo);
$problemMessageJsonView = new ProblemMessageJsonView();
$problemMessageController = new ProblemMessageController($problemMessagePdoRepository, $problemMessageJsonView);

$statusMessagePdoRepository = new PDOStatusMessageRepository($pdo);
$statusMessageJsonView = new StatusMessageJsonView();
$statusMessageController = new StatusMessageController($statusMessagePdoRepository, $statusMessageJsonView);

$problemCommentPdoRepository = new PDOProblemCommentRepository($pdo);
$problemCommentJsonView = new ProblemCommentJsonView();
$problemCommentController = new ProblemCommentController($problemCommentPdoRepository, $problemCommentJsonView);

$problemUrgencyPDORepository = new PDOProblemUrgencyRepository($pdo);
$problemUrgencyJsonView = new ProblemUrgencyJsonView();
$problemUrgencyController = new ProblemUrgencyController($problemUrgencyPDORepository, $problemUrgencyJsonView);

$router = new AltoRouter();
$dev_settings = require_once '../api/src/config/dev_settings.php';
$router->setBasePath($dev_settings['route_path']);

/********* GET *********/

/**
 * @GET
 * @route = locations
 * @return locations
 */
$router->map('GET', '/locations/', function () use (&$locationController) {
    $locationController->handleFindAllLocations();
});

/**
 * @GET
 * @route = location/id
 * @return location
 */
$router->map('GET', '/location/[i:id]/?', function ($id) use (&$locationController) {
    $locationController->handleFindLocationById($id);
});


/**
 * @GET
 * @route = problems
 * @return problems
 */
$router->map('GET', '/problems/', function () use (&$problemMessageController) {
    $problemMessageController->handleFindAllProblems();
});

/**
 * @GET
 * @route = location/id/problems
 * @return problem
 */
$router->map('GET', '/location/[i:id]/problems/', function ($id) use (&$problemMessageController) {
    $problemMessageController->handleFindProblemByLocationId($id);
});

/**
 * @GET
 * @route = problem/id
 * @return problem
 */
$router->map('GET', '/problem/[i:id]/?', function ($id) use (&$problemMessageController) {
    $problemMessageController->handleFindProblemById($id);
});

/**
 * @GET
 * @route = problem/id/comments/
 * @return comments
 */
$router->map('GET', '/problem/[i:id]/comments/', function ($id) use (&$problemCommentController, &$problemMessageController) {
    $problem = $problemMessageController->getProblemWithId($id);

    $problemCommentController->handleFindProblemCommentsById($id, $problem);
});

/**
 * @GET
 * @route = status/id
 * @return status
 */
$router->map('GET', '/status/[i:id]/?', function ($id) use (&$statusMessageController) {
    $statusMessageController->handleFindStatusById($id);
});

/**
 * @GET
 * @route = location/id/status
 * @return status
 */
$router->map('GET', '/location/[i:id]/statuses/', function ($id) use (&$statusMessageController) {
    $statusMessageController->handleFindStatusByLocationId($id);
});

/**
 * @GET
 * @route = problem/id/urgency/
 * @return urgency
 */
$router->map('GET', '/problem/[i:id]/urgency/', function ($id) use (&$problemUrgencyController, &$problemMessageController) {
    $problem = $problemMessageController->getProblemWithId($id);

    $problemUrgencyController->handleFindProblemUrgencyByProblemId($id, $problem);
});

/********* POST *********/

/**
 * @POST
 * @route = problem
 * @return problem
 */
$router->map('POST', '/problem/?', function () use (&$problemMessageController) {
    // Get json objects
    $requestBody = file_get_contents('php://input');
    $data = (array)json_decode($requestBody);
    // variable declaration
    $problemMessage = new ProblemMessage(null,
        $data[0]->problem->problemMessageLocationId,
        $data[0]->problem->problemMessageText,
        $data[0]->problem->problemMessageDate,
        $data[0]->problem->problemMessageIsFinished);

    $problemMessageController->handleInsertProblemMessage($problemMessage);
});

/**
 * @POST
 * @route = comment
 * @return comment
 */
$router->map('POST', '/comment/?', function () use (&$problemCommentController) {
    // Get json objects
    $requestBody = file_get_contents('php://input');
    $data = (array)json_decode($requestBody);
    // variable declaration
    $problemComment = new ProblemComment(null,
        $data[0]->comment->problemMessageId,
        $data[0]->comment->problemCommentText,
        $data[0]->comment->problemCommentDate);

    $problemCommentController->handleInsertProblemComment($problemComment);
});

/**
 * @POST
 * @route = status
 * @return status
 */
$router->map('POST', '/status/?', function () use (&$statusMessageController) {
    // Get json objects
    $requestBody = file_get_contents('php://input');
    $data = (array)json_decode($requestBody);
    // variable declaration

    $status = new StatusMessage(null,
            $data[0]->status->statusLocationId,
            $data[0]->status->statusState,
            $data[0]->status->statusDate
        );

    $statusMessageController->handleInsertStatus($status);
});

/********* PUT *********/

/**
 * @PUT
 * @route = problem/id/urgency/
 * @return urgency
 */
$router->map('POST', '/problemUrgency/?', function () use (&$problemUrgencyController) {

    // Get json objects
    $requestBody = file_get_contents('php://input');
    $data = (array)json_decode($requestBody);
    // variable declaration

    $problemUrgency = new ProblemUrgency(null,
        $data[0]->problemUrgency->problemId,
        $data[0]->problemUrgency->urgencyLevel
    );

    $problemUrgencyController->handleUpdateProblemUrgency($problemUrgency);
});

$match = $router->match();

if( $match && is_callable( $match['target'] ) ) {
    call_user_func_array( $match['target'], $match['params'] );
} else {
    // no route was matched
    //header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
    http_response_code(404);
    ?>
    <!-- <h3>Current request: </h3>
    <pre>
        Target: <?php //var_dump($match['target']); ?>
        Params: <?php //var_dump($match['params']); ?>
        Name: 	<?php //var_dump($match['name']); ?>
    </pre> -->

<?php
}
?>


