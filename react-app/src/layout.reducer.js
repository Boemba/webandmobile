const initialState = {
    locationEntries: [],
    selectedLocation: 1,
    problemListOfLocation: [],
    statusListOfLocation: [],
    problemDetails: {},
    problemUrgency: {},
};

const layoutreducer = (state = initialState, action) => {
    switch (action.type) {
        case 'locationEntries':
            return Object.assign({}, state, { locationEntries: action.data });
        case 'locationSelected':
            console.log("Logged location id on layout.reducer" + action.data);
            return Object.assign({}, state, { selectedLocation: action.data });
        case 'ProblemListOfSelectedLocation':
            return Object.assign({}, state, { problemListOfLocation: action.data });
        case 'StatusListOfSelectedLocation':
            return Object.assign({}, state, { statusListOfLocation: action.data });
        case 'AddProblemEntry':
            return {
                ...state,
                problemListOfLocation: [...state.problemListOfLocation, action.data]
            };
        case 'AddStatusEntry':
            return {
                ...state,
                statusListOfLocation: [...state.statusListOfLocation, action.data]
            };
        case 'SetCurrentProblem':
            return {
                ...state,
                problemDetails: [...state.problemDetails, action.data]
            };
        case 'AddCommentEntry':
            var commentEntry = action.data;
            var clonedProblemDetails = state.problemDetails;
            clonedProblemDetails[0].problemComment.push(commentEntry);
            return Object.assign({}, state, { problemDetails: clonedProblemDetails });
        case 'UpdateProblemUrgency':
            return Object.assign({}, state, { problemUrgency: action.data });
        default:
            return state;
    }
}

export default layoutreducer;

