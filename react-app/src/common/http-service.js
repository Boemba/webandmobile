import axios from 'axios';

class HttpService {
    baseUrl = 'http://localhost:8079/webandmobile/api/api.php';

    getLocations() {
        return axios.get(`${this.baseUrl}/locations/`).then(r => r.data);
    }

    getProblems(locationId = 1) {
        return axios.get(`${this.baseUrl}/location/${locationId}/problems/`).then(r => r.data);
    }

    getProblemUrgencyById(problemId) {
        return axios.get(`${this.baseUrl}/problem/${problemId}/urgency/`).then(r => r.data);
    }

    updateProblemUrgency(problemUrgency) {
        console.log(JSON.stringify(Array({problemUrgency})));
        return axios.post(`${this.baseUrl}/problemUrgency/`, JSON.stringify(Array({ problemUrgency })));
    }
    
    getProblemWithComments(problemId) {
        return axios.get(`${this.baseUrl}/problem/${problemId}/comments/`).then(r => r.data);
    }

    getStatuses(locationId = 1) {
        return axios.get(`${this.baseUrl}/location/${locationId}/statuses/`).then(r => r.data);
    }

    addProblemEntry(problem) {
        console.log(JSON.stringify(Array({ problem })));
        return axios.post(`${this.baseUrl}/problem/`, JSON.stringify(Array({ problem })));
    }

    addCommentEntry(comment) {
        console.log(JSON.stringify(Array({ comment })));
        return axios.post(`${this.baseUrl}/comment/`, JSON.stringify(Array({ comment })));
    }

    addStatusEntry(status) {
        console.log(JSON.stringify(Array({ status })));
        return axios.post(`${this.baseUrl}/status/`, JSON.stringify(Array({ status })));
    }
}

const httpService = new HttpService(1);

export default httpService;