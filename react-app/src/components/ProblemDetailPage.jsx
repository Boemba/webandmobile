import React from 'react';
import HttpService from '../common/http-service';
import { Card, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import Paper from 'material-ui/Paper';
import Comment from './Comment';
import { List } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Store from '../common/store';
import CommentMessageBox from './AddCommentMessageBox';

let hasFetchedProblemEntries = false, hasFetchedUrgencyLevel = false;

class ProblemDetailPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            locationId: props.match.params.locationId,
            problemId: props.match.params.problemId,
            problem: Object(),
            urgencyLevel: 0
        }
    }
    componentWillMount() {
        if (!hasFetchedProblemEntries) {
            HttpService.getProblemWithComments(this.state.problemId).then(fetchedEntries =>
                Store.dispatch({ type: 'SetCurrentProblem', data: fetchedEntries }) &&
                this.setState({problem : fetchedEntries}));
            hasFetchedProblemEntries = true;
        }

        if (!hasFetchedUrgencyLevel) {
            HttpService.getProblemUrgencyById(this.state.problemId).then(fetchedEntry =>
            Store.dispatch({ type: 'UpdateProblemUrgency', data: fetchedEntry.problemUrgency.urgencyLevel }) &&
                this.setState({ urgencyLevel: fetchedEntry.problemUrgency.urgencyLevel }));
        }

        this.unsubscribe = Store.subscribe(() => {
            if(Store.getState().problemDetails !== undefined) {
                this.setState({problem: Store.getState().problemDetails[0]});
                console.log(Store.getState.problemDetails);
            }
            if(Store.getState().problemUrgency !== null) {
                this.setState({urgencyLevel : Store.getState().problemUrgency});
            }
        });
    }

    handleChange = (event, index, value)  => {
        class ProblemUrgency {
            constructor(problem_id, urgency_level) {
                this.problemId = problem_id;
                this.urgencyLevel = urgency_level;
            }
        }
        
        const problemUrgency = new ProblemUrgency(this.state.problemId, value);

        console.log("we komen tot hier");

        HttpService.updateProblemUrgency(problemUrgency).then((fetchedProblemUrgency) => {
            Store.dispatch({ type: 'UpdateProblemUrgency', data: problemUrgency.urgencyLevel });
            console.log("This is fetchedurgency: ");
            console.log(fetchedProblemUrgency);
        });
    }

    render() {
        var commentNodes = "";
        if (this.state.problem !== undefined && this.state.problem.problemComment !== undefined) {
            commentNodes = this.state.problem.problemComment.map((comment) => {
                return (
                    <Comment key={comment.id} comment={comment} />
                );
            });
        }
        return (
            <Paper style={{
                display: 'flex',
                flexFlow: 'row wrap',
                flexShrink: '1',
                flexBasis: 'auto',
                maxWidth: 1200,
                width: '100%',
                margin: '30px auto 30px',
            }}>
                <Card>
                    <CardHeader
                        title={`Problem #${this.state.problem.problemId}`}
                        subtitle={`Created on ${this.state.problem.problemDate}`}
                    />
                    <div>
                        <CardMedia
                            overlay={<CardTitle title={`Problem State: ${this.state.problem.problemIsFinished === 'Yes' ? 'Finished' : 'In Queue'}`} subtitle={`Created on #${this.state.problem.problemDate}`} />}>
                            <img src="https://d3tvpxjako9ywy.cloudfront.net/blog/content/uploads/2017/03/PREVIEW-problem-solving-techniques-624x426.jpg?av=4f79820a5ff37abaf683f64fdfd301d5" alt="cardMedia" />
                        </CardMedia>
                    </div>
                    <CardTitle title={`Problem #${this.state.problem.problemId}`} subtitle={`Created on #${this.state.problem.problemDate} - Urgency ${this.state.urgencyLevel}`} />
                    <CardText>
                        {this.state.problem.problemText} <br />
                        <DropDownMenu value={this.state.urgencyLevel} onChange={this.handleChange}>
                            <MenuItem value={0} primaryText="Urgency level is: 0" />
                            <MenuItem value={1} primaryText="Urgency level is: 1" />
                            <MenuItem value={2} primaryText="Urgency level is: 2" />
                            <MenuItem value={3} primaryText="Urgency level is: 3" />
                            <MenuItem value={4} primaryText="Urgency level is: 4" />
                        </DropDownMenu>
                    </CardText>
                    <Paper style={{
                        display: 'flex',
                        flexFlow: 'row wrap',
                        flexShrink: '1',
                        flexBasis: 'auto',
                        maxWidth: '90%',
                        width: '100%',
                        margin: '30px auto',
                    }}
                        zDepth={1}>
                        <List>
                            <Subheader>{commentNodes.length} Comments</Subheader>
                            {commentNodes}
                            <CommentMessageBox problemId={this.state.problemId} />
                        </List>
                    </Paper>
                </Card>
            </Paper>
        )
    }
}

export default ProblemDetailPage;