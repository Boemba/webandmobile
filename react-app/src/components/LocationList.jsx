import React from 'react';
import Location from './Location';
import { Card, List, CardTitle } from 'material-ui';
import HttpService from '../common/http-service';

let hasFetchedLocationEntries = false;

class LocationList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            locations: [],
        };
    }
    componentWillMount() {
        if(!hasFetchedLocationEntries) {
            HttpService.getLocations().then(fetchedEntries => this.setState({locations : fetchedEntries}));
            hasFetchedLocationEntries = true;
        }
    }
    render() {
        var locationNodes = this.state.locations.map((location) => {
            return (
                <Location key={location.id} location={location} />
            );
        });

        return (
            <Card style={{
                flexGrow: 1,
                alignSelf: 'flex-start' 
            }}>
            <CardTitle title="Locations" />
                <List>
                    {locationNodes}
                </List>
            </Card>
        )
    }
}

export default LocationList;