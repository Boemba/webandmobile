import React from 'react';
import Layout from './Layout';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import '../App.css';
import { Provider } from 'react-redux';
import store from '../common/store';

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <MuiThemeProvider>
                    <Layout />
                </MuiThemeProvider>
            </Provider>
        )
    }
}

export default App;