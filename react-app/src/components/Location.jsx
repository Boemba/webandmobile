import React from 'react';
import ListItem from 'material-ui/List/ListItem';
import Store from '../common/store';
import HttpService from '../common/http-service';


class Location extends React.Component {
    onClick() {
        HttpService.getProblems(this.props.location.id)
            .then((fetchedEntries) => Store.dispatch({ type: 'ProblemListOfSelectedLocation', data: fetchedEntries }));
        HttpService.getStatuses(this.props.location.id)
            .then((fetchedEntries) => Store.dispatch({ type: 'StatusListOfSelectedLocation', data: fetchedEntries }));
        Store.dispatch({ type: 'locationSelected', data: this.props.location.id });
    }
    render() {
        return (
            <ListItem onClick={this.onClick.bind(this)}>
                {this.props.location.name}
            </ListItem>
        );
    }
}

export default Location;