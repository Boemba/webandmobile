import React from 'react';
import ListItem from 'material-ui/List/ListItem';

class Status extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status : props.status
        }
    }

    render() {
        return (
            <ListItem key={this.state.status.statusId}>
                {this.state.status.statusState} <text style={{float: 'right'}}>{this.state.status.statusDate}</text>
            </ListItem>
        );
    }
}

export default Status;