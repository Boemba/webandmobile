import React from 'react';
import ListItem from 'material-ui/List/ListItem';
import { Link } from 'react-router-dom';
import HttpService from '../common/http-service';

let hasFetchedUrgencyLevel = false;

class Problem extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            problem : props.problem,
            problemUrgency : 0,
        }
    }
    componentWillMount() {
        if(!hasFetchedUrgencyLevel) {
                HttpService.getProblemUrgencyById(this.state.problem.problemId).then(fetchedEntry =>
                this.setState({problemUrgency : fetchedEntry.problemUrgency.urgencyLevel}));        
        }
    }
    render() {
        return (
            <ListItem>
                <Link style={{color: 'inherit', textDecoration: 'none'}} to={`/location/${this.props.problem.problemLocationId}/problem/${this.props.problem.problemId}`}>
                    {this.state.problemUrgency} - {this.props.problem.problemText}<text style={{float: 'right'}}>{this.props.problem.problemDate}</text>
                </Link>
            </ListItem>
        );
    }
}

export default Problem;