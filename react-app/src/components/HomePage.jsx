import React from 'react';
import LocationList from './LocationList';
import ProblemList from './ProblemList';
import StatusList from './StatusList';

class ProblemDetailPage extends React.Component {
    render() {
        return (
            <div style={{
                display: 'flex',
                flexFlow: 'row wrap',
                flexShrink: '1',
                flexBasis: 'auto',
                maxWidth: 1200,
                width: '100%',
                margin: '30px auto 30px'
            }}>
                <LocationList />
                <ProblemList />
                <StatusList />
            </div>
        )
    }
}

export default ProblemDetailPage;