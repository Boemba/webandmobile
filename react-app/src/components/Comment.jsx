import React from 'react';
import { ListItem } from 'material-ui/List';


class Comment extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            comment : props.comment,
        }
    }
    render() {
        return (
            <ListItem>
                {this.state.comment.problemDate} - {this.state.comment.problemText}
            </ListItem>
        )
    }
}

export default Comment;