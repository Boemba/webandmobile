import React from 'react';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';
import AppBar from 'material-ui/AppBar';
import HomePage from './HomePage';
import ProblemDetailPage from './ProblemDetailPage';
import '../App.css';

class App extends React.Component {
    render() {
        return (
            <Router>
                <div>
                <AppBar title="React Project" />
                <Route exact path='/' component={HomePage} />
                <Route exact path='/location/:locationId/problem/:problemId' component={ProblemDetailPage} />
                </div>
            </Router>
        )
    }
}

export default App;