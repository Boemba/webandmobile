import React from 'react';
import Card from 'material-ui/Card/Card';
import HttpService from '../common/http-service';
import moment from 'moment';
import Store from '../common/store';

class ProblemMessageBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: ''
        }
    }

    onChange(evt) {
        this.setState({
            message: evt.target.value
        });
    }

    onKeyUp(evt) {
        if (evt.keyCode === 13) {
            evt.preventDefault();
            this.setState({
                message: ''
            });

            class Problem {
                constructor(problemLocationId, problemText) {
                    this.problemMessageLocationId = problemLocationId;
                    this.problemMessageText = problemText;
                    this.problemMessageDate = moment();
                    this.problemMessageIsFinished = 'No';
                }
            }

            const problem = new Problem(Store.getState().selectedLocation, evt.target.value);


            HttpService.addProblemEntry(problem).then((fetchedProblem) => {
                Store.dispatch({ type: 'AddProblemEntry', data: fetchedProblem.data });
            });

            console.log('sent a new message', evt.target.value);
        }
    }

    render() {
        return (
            <Card style={{
                maxWidth: 1200,
                margin: '30px auto 0px',
                padding: 30
            }}>
                <h5>Add a problem: </h5>
                <textarea
                    value={this.state.message}
                    onChange={this.onChange.bind(this)}
                    onKeyUp={this.onKeyUp.bind(this)}
                    style={{
                        width: '100%',
                        border: '1px solid',
                        borderColor: '#D0D0D0',
                        resize: 'none',
                        borderRadius: 2,
                        minHeight: 50,
                        color: '#555',
                        fontSize: 14,
                        outline: 'auto 0px'
                    }} />
            </Card>
        );
    }
}

export default ProblemMessageBox;