import React from 'react';
import Problem from './Problem';
import ProblemMessageBox from './AddProblemMessageBox';
import { Card, List, CardTitle, ListItem } from 'material-ui';
import HttpService from '../common/http-service';
import Store from '../common/store';

let hasFetchedProblemEntries = false;

class ProblemList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            problems: []
        };
    }
    componentWillMount() {
        if (!hasFetchedProblemEntries) {
            HttpService.getProblems().then(fetchedEntries => this.setState({ problems: fetchedEntries }));
            hasFetchedProblemEntries = true;
        }
        this.unsubscribe = Store.subscribe(() => {
            if(Store.getState().problemListOfLocation.length !== undefined) {
                this.setState({problems: Store.getState().problemListOfLocation});
            } else {
                this.setState({problems: []});
            }
        });
    }
    render() {
        var problemNodes;
        
        if(this.state.problems.length !== 0) {
            problemNodes = this.state.problems.map((problem) => {
                return (
                    <Problem key={problem.problemId} problem={problem} />
                );
            });
        } else {
            problemNodes = <ListItem>No problems found for the selected location</ListItem>
        }

        return (
            <Card id="cardList" style={{
                flexGrow: 2,
                marginLeft: 30,
                alignSelf: 'flex-start'
            }}>
                <CardTitle title="Problem messsages" />
                <List>
                    {problemNodes}
                </List>
                <ProblemMessageBox />
            </Card>
        )
    }
}

export default ProblemList;