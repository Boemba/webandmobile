import React from 'react';
import HttpService from '../common/http-service';
import moment from 'moment';
import Store from '../common/store';

class CommentMessageBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: '',
            problemId: props.problemId
        }
    }

    handleChange = (e) => {
        this.setState({
            comment: e.target.value
        });
    }

    handleKeyUp = (e) => {
        if (e.keyCode === 13) {

            class Comment {
                constructor(problemId, commentText) {
                    this.problemMessageId = problemId;
                    this.problemCommentText = commentText;
                    this.problemCommentDate = moment().format("YYYY-MM-DD");
                }
            }

            const comment = new Comment(this.state.problemId, e.target.value);

            HttpService.addCommentEntry(comment).then((fetchedComment) => {
                Store.dispatch({ type: 'AddCommentEntry', data: fetchedComment.data.comment[0] });
            });

            console.log('sent a new comment', e.target.value);

            this.setState({
                comment: ""
            });
        }
    }

    render() {
        return (
            <div style={{
                maxWidth: 1200,
                margin: '0 auto 0',
                padding: 30
            }}>
            <h5>Comment: </h5>
                <textarea
                value={this.state.comment}
                onChange={ this.handleChange }
                onKeyUp={ this.handleKeyUp }
                style={{
                width: '100%',
                border: '1px solid',
                borderColor: '#D0D0D0',
                resize: 'none',
                borderRadius: 2,
                minHeight: 50,
                color: '#555',
                fontSize: 14,
                outline: 'auto 0px'
            }} />
            </div>
        );
    }
}

export default CommentMessageBox;