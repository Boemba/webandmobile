import React from 'react';
import Status from './Status';
import StatusMessageBox from './AddStatusMessageBox';
import { Card, List, CardTitle, ListItem } from 'material-ui';
import HttpService from '../common/http-service';
import Store from '../common/store';

let hasFetechedStatusEntries = false;

class StatusList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            statuses: []
        };
    }
    componentWillMount() {
        if(!hasFetechedStatusEntries) {
            HttpService.getStatuses().then(fetchedEntries => this.setState({statuses : fetchedEntries}));
            hasFetechedStatusEntries = true;

            this.unsubscribe = Store.subscribe(() => {
                if(Store.getState().statusListOfLocation.length !== undefined) {
                    this.setState({statuses: Store.getState().statusListOfLocation});
                } else {
                    this.setState({statuses: []});
                }
            });
        }
    }
    render() {
        var statusNodes;
        if(this.state.statuses.length !== 0) {
            statusNodes = this.state.statuses.map((status) => {
                return (
                    <Status key={status.statusId} status={status} />
                );
            });
        } else {
            statusNodes = <ListItem>No statuses found for the selected location</ListItem>;
        }

        return (
            <Card id="cardList" style={{
                flexGrow: 2,
                marginLeft: 30,
                alignSelf: 'flex-start'
            }}>
            <CardTitle title="Status messages" />
                <List>
                    {statusNodes}
                </List>
                <StatusMessageBox />
            </Card>
        )
    }
}

export default StatusList;